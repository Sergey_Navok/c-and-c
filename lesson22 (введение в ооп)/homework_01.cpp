#include<stdio.h>
#include<conio.h>
#include<math.h>
#include<stdlib.h>
#include<time.h>
#include<windows.h>
#include<string.h>

class MyTriangle {
	private:
	double a;
	double b;
	double c;
	
	public:
	MyTriangle(double a, double b, double c){
		this->a=a;
		this->b=b;
		this->c=c;
	}
	
	void setA(double a){
		this->a=a;
	}
	
	void setB(double b){
		this->b=b;
	}
	
	void setC(double c){
		this->c=c;
	}
	
	double getA(){
		return this->a;
	}
	
	double getB(){
		return this->b;
	}
	
	double getC(){
		return this->c;
	}
	
	double plochad(){
		return ((a+b+c)/2);
	}
	
	double perimeter(){
		return (a+b+c);
	}
	
	void info(){
		printf("������� ������������: %.2lf\n", this->plochad());
		printf("�������� ������������: %.2lf\n", this->perimeter());
		this->ravnobedrenniy();
		this->ravnostoronniy();
	}
	
	void ravnostoronniy(){
		if (this->a==this->b && this->b==this->c && this->a==this->c) {
			printf("����������� �������� ��������������\n");
		}
	}
	
	void ravnobedrenniy(){
		if (this->a==this->b || this->b==this->c ||this->a==this->c){
			printf("����������� �������� ��������������\n");
		}
	}
	
	~MyTriangle(){//����������
		//printf("������������ ������ � ��������\n");
	}
};

int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	double a1, b1, c1, fExit=1;	
	do {
		printf("������� ������� ������������:\n");
		scanf("%lf%lf%lf", &a1, &b1, &c1);
		MyTriangle t1(a1, b1, c1);
		t1.info();

		printf("������� 0, ����� ��������� ������ ��� ������ �����, ����� ����������\n");
		scanf("%lf", &a1);
		if (a1==0) {
			fExit=0;
		}
		system("cls");
	} while(fExit!=0);
	printf("������ ��������� ���������. ������� ����� �������\n");
	
	getch();
	return 0;
}
