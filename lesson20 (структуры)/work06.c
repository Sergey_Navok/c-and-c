#include<stdio.h>
#include<conio.h>
#include<math.h>
#include<stdlib.h>
#include<time.h>
#include<windows.h>
#include<string.h>

struct Human//����� ����������������� ��� ������
{
	char * name;//���� ��� �������� ��� ���������
	int age;
	double salary;
};

void trimStr(char*);
//������������ ������ ������������ ��������� � ������������ ����� (name)

int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	const int MAX_NAME_SIZE=16;
	
	struct Human ** humans=NULL;
	
	int size, i;
	char tmpName[MAX_NAME_SIZE];
	
	printf("������� ���������� �����:\n");
	scanf("%d", &size);
	while(size<0)
	{
		printf("������������ ����, ���������� ��� ���!\n");
		scanf("%d", &size);
	}
	
	humans=(struct Human**)malloc(size*sizeof(struct Human));//������������ ��������� ������ ��� �������� ������� ���������� (������ ������� ��
	//size ���������� struct Human*) � ������ ������ ������� ���������� � �������� ��������� �� ���������
	
	for(i=0; i<size; i++)
	{
		printf("������� ���� � %d ��������:\n", i+1);
		humans[i]=(struct Human*)malloc(1*sizeof(struct Human));//��������� ����� ������, ������������ ��� �������� ���� ����� ������
		//�������� ��������� struct Human � ������ ������, ����������� ����� ������, � ��������������� ������� (i) �� ������� ���������� struct Human*
		printf("������� ���:\n");
		fflush(stdin);
		fgets(tmpName, MAX_NAME_SIZE, stdin);
		trimStr(tmpName);
		humans[i]->name=(char*)malloc((strlen(tmpName)+1)*sizeof(char));
		strcpy(humans[i]->name, tmpName);
		
		printf("������� �������:\n");
		scanf("%d", &humans[i]->age);
		
		printf("������� ��������:\n");
		scanf("%lf", &humans[i]->salary);
	}
	
	printf("���� � �����:\n");
	for(i=0; i<size; i++)
	{
		printf("|%2d|%16s|%3d|%7.2lf|\n", i+1, humans[i]->name, humans[i]->age, humans[i]->salary);
	}
	
	for(i=0; i<size; i++)
	{
		free(humans[i]->name);
		free(humans[i]);
	}
	
	free(humans);
	humans=NULL;
	
	getch();
	return 0;
}

void trimStr(char * str)
{
	if(str[strlen(str)-1]=='\n')
		str[strlen(str)-1]='\0';
}
