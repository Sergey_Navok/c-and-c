#include<stdio.h>
#include<conio.h>
#include<math.h>
#include<stdlib.h>
#include<time.h>
#include<windows.h>
#include<string.h>
/*
���� ������ � ���� �������� ��������� ����-�� � ������� ���������, �� ����� ��������� ��������� ���: ���������->����
*/


struct Human//����� ����������������� ��� ������
{
	char * name;//���� ��� �������� ��� ���������
	int age;
	double salary;
};

void trimStr(char*);
//����������� ������ ����������� ��������� � ������������ �����

int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	
	const int MAX_NAME_SIZE=16, MAX_SIZE=10;
	struct Human humans[MAX_SIZE];
	char tmp[MAX_NAME_SIZE];
	int size, i;
	
	printf("������� ���������� �����:\n");
	scanf("%d", &size);
	while(size<0 || size>MAX_SIZE)
	{
		printf("������������ ����, ���������� ��� ���!\n");
		scanf("%d", &size);
	}
	for(i=0; i<size; i++)
	{
		printf("������� ���� � %d ��������:\n", i+1);
		printf("������� ���:\n");
		fflush(stdin);
		fgets(tmp, MAX_NAME_SIZE, stdin);
		trimStr(tmp);
		humans[i].name=(char*)malloc((strlen(tmp)+1)*sizeof(char));
		strcpy(humans[i].name, tmp);
		
		printf("������� �������:\n");
		scanf("%d", &humans[i].age);
		
		printf("������� ��������:\n");
		scanf("%lf", &humans[i].salary);
	}
	
	printf("���� � ��������:\n");
	
	for(i=0; i<size; i++)
	{
		printf("|%2d|%16s|%3d|%7.2lf|\n", i+1, humans[i].name, humans[i].age, humans[i].salary);
	}
	
	for(i=0; i<size; i++)
	{
		free(humans[i].name);
		humans[i].name=NULL;
	}
	
	
	getch();
	return 0;
}

void trimStr(char * str)
{
	if(str[strlen(str)-1]=='\n')
		str[strlen(str)-1]='\0';
}
