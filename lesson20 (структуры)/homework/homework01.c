#include<stdio.h>
#include<conio.h>
#include<math.h>
#include<stdlib.h>
#include<time.h>
#include<windows.h>
#include<string.h>
/*
1.  �������� ���������� � ������ (���������� ����� � ����� ��� ������)  n ����������. 
�) ����� ����� ����������, ������� ����� ���� �����. 
�) ��������, ������� �� ���� ���� ��������, ����� �������� ������� �� ����� ���� ����� ����� 25 ��. 
�) ����� ����� ����������, ���������� ����� ������� ����������� ������� ����� ����� ���� ����������.
*/

struct Bag
{
	int quantity;
	int weight;
};

int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	
	struct Bag ** bags=NULL;
	int i, size, counter=0;
	double average;
	
	printf("������� ���������� ����������:\n");
	scanf("%d", &size);
	while(size<0)
	{
		printf("������������ ����, ���������� ��� ���!\n");
		scanf("%d", &size);
	}
	
	bags=(struct Bag**)malloc(size*sizeof(struct Bag));
	
	for(i=0; i<size; i++)
	{
		bags[i]=(struct Bag*)malloc(1*sizeof(struct Bag));
		printf("������� ���������� ����� %d-�� ���������:\n", i+1);
		scanf("%d", &bags[i]->quantity);
		
		printf("������� ����� ��� ������:\n");
		scanf("%d", &bags[i]->weight);
	}
	
	for(i=0; i<size; i++)
	{
		if(bags[i]->quantity > 2) {
			counter++;
		}
	}
	printf("����� ����������, � ������� ����� ���� �����: %d\n", counter);
	
	
	counter=0;
	for(i=0; i<size; i++)
	{
		if((bags[i]->quantity < 2) && (bags[i]->weight < 25)) {
			counter++;
		}
	}
	printf("%d ��������(�) ������ ���� ���� ����� ����� 25��\n", counter);
	
	counter=0;
	for(i=0; i<size; i++)
	{
		counter+=bags[i]->quantity;
	}
	average=counter/size;
	printf("� ������� �� ������ ��������� ����������: %.2lf �����\n", average);
	counter=0;
	for(i=0; i<size; i++)
	{
		if(bags[i]->quantity >= average) {
			counter++;
		}
	}
	printf("%d ��������(�) ������ ����� ������ ��������\n", counter);
	
	for(i=0; i<size; i++)
	{
		free(bags[i]);
	}
	
	free(bags);
	bags=NULL;
	
	getch();
	return 0;
}
