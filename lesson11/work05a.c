#include<stdio.h>
#include<conio.h>
#include<locale.h>
#include<math.h>
#include<stdlib.h>
#include<time.h>

/*
5.  ���� ��� ������� ������ �������. �������� ������ ������, ������ ������� �������� �����: 
�) ����� ��������� � ��� �� ������� � �������� ��������; 
�) ������������ ��������� � ��� �� ������� � �������� ��������; 
�) ������������� �� ��������� � ��� �� ������� � �������� ��������. 
*/

main()
{
	setlocale(LC_ALL, "rus");
	const int MAX_SIZE=100;
	int a1[MAX_SIZE], a2[MAX_SIZE], a[MAX_SIZE];
	int i, size;
	
	printf("������� ���������� ��������� ��������:\n");
	scanf("%d", &size);
	
	while(size<1 || size>MAX_SIZE)
	{
		printf("������������ ����\n");
		scanf("%d", &size);
	}
	
	srand(time(0));
	for(i=0; i<size; i++)
	{
		a1[i]=rand()%101-50;
		a2[i]=rand()%101-50;
	}
	
	for(i=0; i<size; i++)
	{
		a[i]=a1[i]+a2[i];
	}
	
	for(i=0; i<size; i++)
	{
		printf("|%2d|%4d|%4d|%4d|\n", i+1, a1[i], a2[i], a[i]);
	}
		
	getch();
}
