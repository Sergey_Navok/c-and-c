#include<stdio.h>
#include<conio.h>
#include<locale.h>
#include<math.h>
#include<stdlib.h>
#include<time.h>

/*
5. ��� ������. ���������� ������� �������������� ������� 5 � ��������� 10.
*/

main()
{
	setlocale(LC_ALL, "rus");
	const int MAX_SIZE=50;//������������ ������
	int a[MAX_SIZE];//���������� �������
	int i, size, sum=0, kol=0;//����������
	double avg;//������� ��������������
	printf("������� ���������� ��������� [1...%d]:\n", MAX_SIZE);
	scanf("%d", &size);//���� ������������� ���������� ���������
	
	//�������� ���������
	while(size<1 || size>MAX_SIZE)
	{
		printf("����������� ����\n");
		scanf("%d", &size);
	}
	
	//���������� ������� ���������� ������� [-10...10]
	srand(time(0));
	for(i=0; i<size; i++)
	{
		a[i]=rand()%20-10;
	}
	
	//����� ������� ���������������� �������
	printf("�������� ������:\n");
	for(i=0; i<size; i++)
	{
		printf("%2d ", a[i]);
	}
	printf("\n");
		
	
	//�������� ���������
	for(i=1; i<size; i++)
	{
		if((a[i]%5==0 || a[i]&10==0) && a[i]!=0)
		{
			sum=sum+a[i];
			kol++;
		}
	}
	
	//������ �������� ���������������
	avg=(double)sum/kol;
	
	//����� �����������
	if(kol!=0)
	{
		printf("������� ��������������, ������� 5 � 10 = %.2lf\n", avg);
		printf("������� ����� ����������� %d ���(�)\n", kol);
	}
	else
	{
		printf("��� �����, ������� 5 ��� 10!");
	}
		
	getch();
}
