#include<stdio.h>
#include<conio.h>
#include<locale.h>
#include<math.h>
#include<stdlib.h>
#include<time.h>

/*
4. � ������� ����� �������� � ������ ����������� �� ������������� ���������.
*/

main()
{
	setlocale(LC_ALL, "rus");
	const int MAX_SIZE=50;//������������ ������
	int a[MAX_SIZE];//���������� �������
	int i, size;
	printf("������� ���������� ��������� [1...%d]:\n", MAX_SIZE);
	scanf("%d", &size);//���� ������������� ���������� ���������
	
	while(size<1 || size>MAX_SIZE)//�������� ���������
	{
		printf("����������� ����\n");
		scanf("%d", &size);
	}
	
	//���������� ������� ���������� ������� [-50...50]
	srand(time(0));
	for(i=0; i<size; i++)
	{
		a[i]=rand()%5-10;//rand()%(50-(-50+1)+(-50));
	}
	
	//����� �������
	printf("�������� ������\n");
	for(i=0; i<size; i++)
	{
		printf("%d ", a[i]);
	}
	printf("\n");
	
	int maxNeg, iFirstNeg, fNeg=0;//0 - ������������� ��������� ���, 1 - ������������� ������� ����
	
	for(i=0; i<size && fNeg==0; i++)//��������� ����� � ������� �����
	{
		if(a[i]<0)
		{
			maxNeg=a[i];
			iFirstNeg=i;
			fNeg=1;
		}
	}
	
	
	
	if(fNeg==1)
	{
		for(i=iFirstNeg+1; i<size; i++)
		{
			if(a[i]<0 && a[i]>maxNeg)
			{
				maxNeg=a[i];
			}
		}
		
		printf("������������ �� �������������: %d\n", maxNeg);
		printf("�������:\n");
		
		for(i=iFirstNeg; i<size; i++)
		{
			if(a[i]==maxNeg)
			{
				printf("%d\n", i);
			}
		}
	}
	else
	{
		printf("��� ������������� ���������\n");
	}
		
	getch();
}
