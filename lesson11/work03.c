#include<stdio.h>
#include<conio.h>
#include<locale.h>
#include<math.h>
#include<stdlib.h>
#include<time.h>

/*
3.  �� ��������� ������� a, ������������ ������ �������, 
������������ ������ b ���� �� ������� �� �������: 
������ �������� ������� a �������, �������� �������� ��� ���������. 
*/

main()
{
	setlocale(LC_ALL, "rus");
	const int MAX_SIZE=100;
	int a1[MAX_SIZE], a2[MAX_SIZE];
	int i, size;
	
	printf("������� ���������� ��������� �������:\n");
	scanf("%d", &size);
		
	while(size<1 || size>MAX_SIZE)
	{
		printf("������������ ����\n");
		scanf("%d", &size);
	}
	
	srand(time(0));
	for(i=0; i<size; i++)
	{
		a1[i]=rand()%101-50;
	}

	printf("�������� ������ ������\n");
	
	srand(time(0));
	for(i=0; i<size; i++)
	{
		printf("%5d ", a1[i]);
	}
	
	printf("\n");
	
	for(i=0; i<size; i++)
	{
		if(a1[i]%2==0)
		{
			a2[i]=a1[i]*2;//a2[i]=a1[i]%2==0?a1[i]*2:a1[i];
		}
		else
		{
			a2[i]=a1[i];
		}
	}
	
	printf("�������������� ������ ������:\n");
	
	for(i=0; i<size; i++)
	{
		printf("%5d ", a2[i]);
	}
	
	getch();
}
