#include<stdio.h>
#include<conio.h>
#include<locale.h>
#include<math.h>
#include<stdlib.h>
#include<time.h>

/*
7. ��������� ������ �� n ��������� ����� ����� �������, 
����� �������� ��������� ��� ��������� ������� ����� ������� ������������:
�) ������������ ������������������;
�) ��������� ������������������.
*/

main()
{
	setlocale(LC_ALL, "rus");
	int i, size;//����������
	const int MAX_SIZE=50;//������������ ������ �������
	int a[MAX_SIZE];//������

	//������������ �������
	printf("������� ���������� ��������� ������� �� 1 �� %d:\n", MAX_SIZE);
	scanf("%d", &size);	
	while(size<1 || size>MAX_SIZE)//�������� ���������
	{
		printf("�������� �������� �� ������������� �������\n");
		scanf("%d", &size);
	}
	
	
	//��������� ��������� �����
	srand(time(0));
	while(i<size)
	{
	a[i]=rand()%10+i*-10;
	printf("�������� ������� %2d ������������� ����� %3d:\n", i+1, a[i]);
	i++;
	}
	
	//����� �������� �������
	printf("\n\n�������� ������ � �������:\n");
	for(i=0; i<size; i++)
	{
		printf("%d ", a[i]);
	}
	printf("\n");

	getch();
}
