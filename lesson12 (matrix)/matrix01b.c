#include<stdio.h>
#include<conio.h>
#include<locale.h>
#include<math.h>
#include<stdlib.h>
#include<time.h>

/*
1.  ��� ��������� ������. 
�) �������� �������� ���� ��������� k ������ ������� ������ 5. 
�) ��������� �������� ���� ��������� s ������� ������� �� 10.
*/

main()
{
	setlocale(LC_ALL, "rus");
	const int maxR=10, maxC=15;//maxR - rolls, maxC - column
	int a[maxR][maxC];
	int sizeR, sizeC, i, j;
	
	printf("������� ���������� ����� [1...%d]:\n", maxR);
	scanf("%d", &sizeR);//���� ������������� ���������� �����
	
	while(sizeR<1 || sizeR>maxR)//�������� ����� ���������� �����
	{
		printf("����������� ����\n");
		scanf("%d", &sizeR);
	}
	
	printf("������� ���������� �������� [1...%d]:\n", maxC);
	scanf("%d", &sizeC);//���� ������������� ���������� ��������
	
	while(sizeC<1 || sizeC>maxC)//�������� ����� ���������� ��������
	{
		printf("����������� ����\n");
		scanf("%d", &sizeC);
	}
	
	srand(time(0));//��������� ��������� �����
	for(i=0; i<sizeR; i++)
	{
		for(j=0; j<sizeC; j++)
		{
			a[i][j]=rand()%101-50;
		}
	}
		
	printf("�������� �������\n");
	for(i=0; i<sizeR; i++)
	{
		for(j=0; j<sizeC; j++)
		{
			printf("%5d", a[i][j]);
		}
		printf("\n");
	}
	
	int num;
	printf("������� ����� �������:\n");
	scanf("%d", &num);
	
	while(num<1 || num>sizeC)
	{
		printf("������������ ����\n");
		scanf("%d", &num);
	}
	
	for(i=0; i<sizeR; i++)
	{
		a[i][num-1]=a[i][num-1]+10;
	}
	
	printf("���������� �������\n");
	for(i=0; i<sizeR; i++)
	{
		for(j=0; j<sizeC; j++)
		{
			printf("%5d", a[i][j]);
		}
		printf("\n");
	}
	
	getch();
}
