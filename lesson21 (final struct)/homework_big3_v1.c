#include<stdio.h>
#include<conio.h>
#include<locale.h>
#include<math.h>
#include<windows.h>
#include<stdlib.h>
#include<time.h>


struct Date
{
   int day;
   int month;
   int year;
};

struct Guest
{
	char * name;
	int room;
	struct Date * dateIn;
	struct Date * dateOut;
};

void trimStr(char*);

int main() 
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	const int MAX_NAME_SIZE=16;
	struct Guest ** guests=NULL;
	int size=5, i, j, sw1, sw2, sw3, fExit=1;
	char tmpName[MAX_NAME_SIZE];
	
	guests=(struct Guest**)malloc(size*sizeof(struct Guest*));
	
	guests[0]=(struct Guest*)malloc(sizeof(struct Guest));
	guests[0]->name=(char*)malloc(5*sizeof(char));
	strcpy(guests[0]->name, "����");
	guests[0]->room=666;
	guests[0]->dateIn=(struct Date*)malloc(sizeof(struct Date));
	guests[0]->dateIn->day=12;
	guests[0]->dateIn->month=1;
	guests[0]->dateIn->year=2021;
	guests[0]->dateOut=(struct Date*)malloc(sizeof(struct Date));
	guests[0]->dateOut->day=30;
	guests[0]->dateOut->month=5;
	guests[0]->dateOut->year=2021;

	guests[1]=(struct Guest*)malloc(sizeof(struct Guest));
	guests[1]->name=(char*)malloc(5*sizeof(char));
	strcpy(guests[1]->name, "����");
	guests[1]->room=555;
	guests[1]->dateIn=(struct Date*)malloc(sizeof(struct Date));
	guests[1]->dateIn->day=20;
	guests[1]->dateIn->month=1;
	guests[1]->dateIn->year=2021;
	guests[1]->dateOut=(struct Date*)malloc(sizeof(struct Date));
	guests[1]->dateOut->day=5;
	guests[1]->dateOut->month=2;
	guests[1]->dateOut->year=2021;
	
	guests[2]=(struct Guest*)malloc(sizeof(struct Guest));
	guests[2]->name=(char*)malloc(5*sizeof(char));
	strcpy(guests[2]->name, "����");
	guests[2]->room=333;
	guests[2]->dateIn=(struct Date*)malloc(sizeof(struct Date));
	guests[2]->dateIn->day=10;
	guests[2]->dateIn->month=3;
	guests[2]->dateIn->year=2020;
	guests[2]->dateOut=(struct Date*)malloc(sizeof(struct Date));
	guests[2]->dateOut->day=25;
	guests[2]->dateOut->month=10;
	guests[2]->dateOut->year=2021;
	
	guests[3]=(struct Guest*)malloc(sizeof(struct Guest));
	guests[3]->name=(char*)malloc(5*sizeof(char));
	strcpy(guests[3]->name, "����");
	guests[3]->room=222;
	guests[3]->dateIn=(struct Date*)malloc(sizeof(struct Date));
	guests[3]->dateIn->day=31;
	guests[3]->dateIn->month=4;
	guests[3]->dateIn->year=2020;
	guests[3]->dateOut=(struct Date*)malloc(sizeof(struct Date));
	guests[3]->dateOut->day=25;
	guests[3]->dateOut->month=5;
	guests[3]->dateOut->year=2020;
	
	guests[4]=(struct Guest*)malloc(sizeof(struct Guest));
	guests[4]->name=(char*)malloc(5*sizeof(char));
	strcpy(guests[4]->name, "����");
	guests[4]->room=111;
	guests[4]->dateIn=(struct Date*)malloc(sizeof(struct Date));
	guests[4]->dateIn->day=1;
	guests[4]->dateIn->month=1;
	guests[4]->dateIn->year=2021;
	guests[4]->dateOut=(struct Date*)malloc(sizeof(struct Date));
	guests[4]->dateOut->day=10;
	guests[4]->dateOut->month=1;
	guests[4]->dateOut->year=2021;
	
	do
	{
		printf("1. ����������:\n");
		printf("2. �����:\n");
		printf("3. �����:\n");
		printf("4. ��������:\n");
		printf("5. ����������:\n");
		printf("6. �����\n");
		printf("�������� ����� ����:\n");
		printf("---------------------------------------\n");
		scanf("%d", &sw1);
		switch(sw1)
		{
			case 1://����������
				{
					printf("�������� ������� ����������:\n");
					printf("1. ���� �������\n");
					printf("2. ��������� �������\n");
					printf("---------------------------------------\n");
					scanf("%d", &sw2);
					switch(sw2)
					{
						case 1://���������� ������
							{
								size++;
								guests=(struct Guest**)realloc(guests, size*sizeof(struct Guest*));//�������� ������ ��� ��������� �� ���������
								guests[size-1]=(struct Guest*)malloc(sizeof(struct Guest));//��������� ����� ������, ������������ ��� �������� ���� ����� ������ 
								//�������� ��������� struct Guest � ������ ������ ����������� ����� � ��������� ���������� ��������� ������� ����������
								
								printf("������� ���:\n");
								fflush(stdin);//�������� ������� �����
								fgets(tmpName, MAX_NAME_SIZE, stdin);
								trimStr(tmpName);
								guests[size-1]->name=(char*)malloc((strlen(tmpName)+1)*sizeof(char));//�������� ������ ��� �������� �����
								strcpy(guests[size-1]->name, tmpName);//�������� ��� �� ��������� ���������� � ���������� ������
								
								printf("������� ����� �������:\n");
								scanf("%d", &guests[size-1]->room);//���������� ����� �������
								
								printf("���� ������\n");
								fflush(stdin);
								guests[size-1]->dateIn=(struct Date*)malloc(sizeof(struct Date));//�������� ������ ��� �������� ����
								printf("������� ���� ������:\n");
								scanf("%d", &guests[size-1]->dateIn->day);
								printf("������� ����� ������:\n");
								scanf("%d", &guests[size-1]->dateIn->month);
								printf("������� ��� ������:\n");
								scanf("%d", &guests[size-1]->dateIn->year);
								
								printf("���� ������\n");
								fflush(stdin);
								guests[size-1]->dateOut=(struct Date*)malloc(sizeof(struct Date));//�������� ������ ��� �������� ����
								printf("������� ���� ������:\n");
								scanf("%d", &guests[size-1]->dateOut->day);
								printf("������� ����� ������:\n");
								scanf("%d", &guests[size-1]->dateOut->month);
								printf("������� ��� ������:\n");
								scanf("%d", &guests[size-1]->dateOut->year);
							}
						break;
						
						case 2://���������� ����������
							{
								printf("������� ������� ��������?\n");
								scanf("%d", &j);
								size=size+j;
								guests=(struct Guest**)realloc(guests, size*sizeof(struct Guest*));
								for(i=size-j; i<size; i++)
								{
									guests[i]=(struct Guest*)malloc(sizeof(struct Guest));//��������� ����� ������, ������������ ��� �������� ���� ����� ������ 
									//�������� ��������� struct Guest � ������ ������ ����������� ����� � ��������� ���������� ��������� ������� ����������
									
									printf("�������	 ���:\n");
									fflush(stdin);//�������� ������� �����
									fgets(tmpName, MAX_NAME_SIZE, stdin);
									trimStr(tmpName);
									guests[i]->name=(char*)malloc((strlen(tmpName)+1)*sizeof(char));//�������� ������ ��� �������� �����
									strcpy(guests[i]->name, tmpName);//�������� ��� �� ��������� ���������� � ���������� ������
									
									printf("������� ����� �������:\n");
									scanf("%d", &guests[i]->room);//���������� ����� �������
									
									printf("���� ������\n");
									fflush(stdin);
									guests[i]->dateIn=(struct Date*)malloc(sizeof(struct Date));//�������� ������ ��� �������� ����
									printf("������� ���� ������:\n");
									scanf("%d", &guests[i]->dateIn->day);
									printf("������� ����� ������:\n");
									scanf("%d", &guests[i]->dateIn->month);
									printf("������� ��� ������:\n");
									scanf("%d", &guests[i]->dateIn->year);
									
									printf("���� ������\n");
									fflush(stdin);
									guests[i]->dateOut=(struct Date*)malloc(sizeof(struct Date));//�������� ������ ��� �������� ����
									printf("������� ���� ������:\n");
									scanf("%d", &guests[i]->dateOut->day);
									printf("������� ����� ������:\n");
									scanf("%d", &guests[i]->dateOut->month);
									printf("������� ��� ������:\n");
									scanf("%d", &guests[i]->dateOut->year);
								}
							}
						break;
						
						default:
							{
								printf("������������ ����� ����!\n");
							}
						break;
					}
				}
			break;
			case 2://�����
				{
					printf("���������� � ������:\n");
					for(i=0; i<size; i++) {
						printf("|%2d|%16s|%4d|%.2d.%.2d.%4d|%.2d.%.2d.%4d|\n", i+1, guests[i]->name, guests[i]->room, guests[i]->dateIn->day, guests[i]->dateIn->month, guests[i]->dateIn->year, guests[i]->dateOut->day, guests[i]->dateOut->month, guests[i]->dateOut->year);
					}
				}
			break;
			
			case 3://�����
				{
					printf("������� ��� ��� ������:\n");
					fflush(stdin);
					fgets(tmpName, MAX_NAME_SIZE, stdin);
					trimStr(tmpName);
					int counter=0;
					for(i=0; i<size; i++)
					{
						if(strcmp(guests[i]->name, tmpName)==0)
						{
							counter++;
							printf("%d) %s\n", i+1, guests[i]->name);
						}
					}
					
					if(counter==0)
					{
						printf("���������� ���\n");
					}
					else
					{
						printf("������� ����������: %d\n", counter);
					}
				}
			break;
			
			case 4://��������
				{
					printf("�������� ������� ��������:\n");
					printf("1. �� ������ ��������\n");
					printf("2. �� �����\n");
					printf("---------------------------------------\n");
					scanf("%d", &sw2);
					switch(sw2)
					{
						case 1:
							{
								printf("������� ���������� ����� ��� ��������:\n");
								scanf("%d", &j);
								free(guests[j-1]);
								for(i=j-1; i<size-1; i++)
								{
									guests[i]=guests[i+1];
								}
								size--;
								guests=(struct Guest**)realloc(guests, size*sizeof(struct Guest*));
							}
						break;
						
						case 2:
							{
								printf("������� ��� ��� ��������:\n");
								fflush(stdin);
								fgets(tmpName, MAX_NAME_SIZE, stdin);
								trimStr(tmpName);
								int counter=0;
								for(i=0; i<size; i++)
								{
									if(strcmp(guests[i]->name, tmpName)==0)
									{
										free(guests[i]);
										counter++;
										for(j=i; j<size-1; j++)
										{
											guests[j]=guests[j+1];
										}
										size--;
										guests=(struct Guest**)realloc(guests, size*sizeof(struct Guest*));
									}
								}
								
								if(counter==0)
								{
									printf("���������� ���\n");
								}
								else
								{
									printf("�������: %d\n", counter);
								}
							}
						break;
						
						default:
							{
								printf("�������� ����� ������ ����\n");
							}
						break;
					}
				}
			break;
			
			case 5://����������
				{
					struct Guest * tmpGuest=NULL;
					printf("�������� �������� ����������:\n");
					printf("1. �� �����\n");
					printf("2. �� ������ �������\n");
					printf("3. �� ���� ������\n");
					printf("4. �� ���� ������\n");
					scanf("%d", &sw2);
					switch(sw2)
					{
						case 1://�� �����
							{
								printf("����������:\n");
								printf("1. � ���������� �������\n");
								printf("2. � �������� �����������\n");
								printf("---------------------------------------\n");
								scanf("%d", &sw3);
								switch(sw3)
								{
									case 1://� ���������� �������
										{
											for(j=0; j<size-1; j++)
											{
												for(i=0; i<size-1-j; i++)
												{
													if(strcmp(guests[i]->name, guests[i+1]->name)>0)
													{
														tmpGuest=guests[i];
														guests[i]=guests[i+1];
														guests[i+1]=tmpGuest;
													}
												}
											}
											tmpGuest=NULL;
										}
									break;
									
									case 2://� �������� �����������
										{
											for(j=0; j<size-1; j++)
											{
												for(i=0; i<size-1-j; i++)
												{
													if(strcmp(guests[i]->name, guests[i+1]->name)<0)
													{
														tmpGuest=guests[i];
														guests[i]=guests[i+1];
														guests[i+1]=tmpGuest;
													}
												}
											}
											tmpGuest=NULL;
										}
									break;
									
									default:
										{
											printf("������������ ����� ������ ����\n");
										}
									break;
								}
							}
						break;
						
						case 2://�� �������
							{
								printf("���������� �� ��������:\n");
								printf("1. �� �����������\n");
								printf("2. �� ��������\n");
								printf("---------------------------------------\n");
								scanf("%d", &sw3);
								switch(sw3)
								{
									case 1:
										{
											for(j=0; j<size-1; j++)
											{
												for(i=0; i<size-1-j; i++)
												{
													if(guests[i]->room > guests[i+1]->room)
													{
														tmpGuest=guests[i];
														guests[i]=guests[i+1];
														guests[i+1]=tmpGuest;
													}
												}
											}
											tmpGuest=NULL;
										}
									break;
									
									case 2:
										{
											for(j=0; j<size-1; j++)
											{
												for(i=0; i<size-1-j; i++)
												{
													if(guests[i]->room < guests[i+1]->room)
													{
														tmpGuest=guests[i];
														guests[i]=guests[i+1];
														guests[i+1]=tmpGuest;
													}
												}
											}
											tmpGuest=NULL;
										}
									break;
									
									default:
										{
											printf("������������ ����� ������ ����\n");
										}
									break;
								}
							}
						break;
						
						case 3://�� ���� ������
							{
								printf("�������� ������ ���������� �� ���� ������\n");
								printf("1. �� ����������� ���\n");
								printf("2. �� �������� ���\n");
								scanf("%d", &sw3);
								switch(sw3)
								{
									case 1://�� �����������
										{
											for(j=0; j<size-1; j++)
											{
												for(i=0; i<size-1-j; i++)
												{
													if(guests[i]->dateIn->year > guests[i+1]->dateIn->year ||
													(guests[i]->dateIn->year == guests[i+1]->dateIn->year && guests[i]->dateIn->month > guests[i+1]->dateIn->month) ||
													(guests[i]->dateIn->year == guests[i+1]->dateIn->year && guests[i]->dateIn->month == guests[i+1]->dateIn->month && guests[i]->dateIn->day > guests[i+1]->dateIn->day))
													{
														tmpGuest=guests[i];
														guests[i]=guests[i+1];
														guests[i+1]=tmpGuest;
													}        
												}
											}
											tmpGuest=NULL;
										}
									break;
									
									case 2://�� ��������
										{
											for(j=0; j<size-1; j++)
											{
												for(i=0; i<size-1-j; i++)
												{
													if(guests[i]->dateIn->year < guests[i+1]->dateIn->year ||
													(guests[i]->dateIn->year == guests[i+1]->dateIn->year && guests[i]->dateIn->month < guests[i+1]->dateIn->month) ||
													(guests[i]->dateIn->year == guests[i+1]->dateIn->year && guests[i]->dateIn->month == guests[i+1]->dateIn->month && guests[i]->dateIn->day < guests[i+1]->dateIn->day))
													{
														tmpGuest=guests[i];
														guests[i]=guests[i+1];
														guests[i+1]=tmpGuest;
													}        
												}
											}
											tmpGuest=NULL;
										}
									break;
									
									default:
										{
											printf("������������ ����� ������ ����\n");
										}
									break;
								}
							}
						break;
						
						case 4://�� ���� ������
							{
								printf("�������� ������ ���������� �� ���� ������\n");
								printf("1. �� ����������� ���\n");
								printf("2. �� �������� ���\n");
								scanf("%d", &sw3);
								switch(sw3)
								{
									case 1://�� �����������
										{
											for(j=0; j<size-1; j++)
											{
												for(i=0; i<size-1-j; i++)
												{
													if(guests[i]->dateOut->year > guests[i+1]->dateOut->year ||
													(guests[i]->dateOut->year == guests[i+1]->dateOut->year && guests[i]->dateOut->month > guests[i+1]->dateOut->month) ||
													(guests[i]->dateOut->year == guests[i+1]->dateOut->year && guests[i]->dateOut->month == guests[i+1]->dateOut->month && guests[i]->dateOut->day > guests[i+1]->dateOut->day))
													{
														tmpGuest=guests[i];
														guests[i]=guests[i+1];
														guests[i+1]=tmpGuest;
													}        
												}
											}
											tmpGuest=NULL;
										}
									break;
									
									case 2://�� ��������
										{
											for(j=0; j<size-1; j++)
											{
												for(i=0; i<size-1-j; i++)
												{
													if(guests[i]->dateOut->year < guests[i+1]->dateOut->year ||
													(guests[i]->dateOut->year == guests[i+1]->dateOut->year && guests[i]->dateOut->month < guests[i+1]->dateOut->month) ||
													(guests[i]->dateOut->year == guests[i+1]->dateOut->year && guests[i]->dateOut->month == guests[i+1]->dateOut->month && guests[i]->dateOut->day < guests[i+1]->dateOut->day))
													{
														tmpGuest=guests[i];
														guests[i]=guests[i+1];
														guests[i+1]=tmpGuest;
													}        
												}
											}
											tmpGuest=NULL;
										}
									break;
									
									default:
										{
											printf("������������ ����� ������ ����\n");
										}
									break;
								}
							}
						break;
						
						default:
							{
								printf("�������� ����� ������ ����\n");
							}
						break;
					}
				}
			break;
			
			case 6://�����
				{
					fExit=0;
					for(i=0; i<size; i++)
					{
						free(guests[i]->name);
						free(guests[i]->dateIn);
						free(guests[i]->dateOut);
						free(guests[i]);
					}
					free(guests);
					guests=NULL;
				}
			break;
			default:
				{
					printf("������������ ����� ������ ����\n");
				}
			break;
		}
		printf("������� ����� �������\n");
		getch();
		system("cls");
		
	}while(fExit==1);
	
	printf("������ ��������� ���������! ������� ����� �������:\n");
	getch();
	return 0;
}

void trimStr(char * str)
{
	if(str[strlen(str)-1]=='\n')
		str[strlen(str)-1]='\0';
}
