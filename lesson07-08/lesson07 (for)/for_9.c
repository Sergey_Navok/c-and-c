#include<stdio.h>
#include<conio.h>
#include<locale.h>
#include<math.h>

/*
9. ��������� 1 ������ ������ ���� � �����, ������ n ���.
����� ������ ����� ������ ������ ������������� �� 2% �� ��������� �����. 
���������� ������� ����� ������ � ����� ������ �� ������ ����� ����� ����.
C������ �������������� ���� ����� ����� �� a �� b (�������� a � b �������� � ����������; b>= a).
*/

main()
{
	setlocale(LC_ALL, "rus");
	double sum, addSum, koef=0.02;
	int i;
	printf("������� ������ ����� ������:\n");
	scanf("%lf", &sum);
	
	if(sum>0)
	{
		for(i=1; i<=12; i++)
		{
			addSum=sum*koef;
			sum=sum+addSum;
			printf("�� ����� %2d ������ �������: %.2lf�, �����: %7.2lf\n", i, addSum, sum);
		}
	}
	else
	{
		printf("������������ ����\n");
	}
	
	getch();
}
