#include<stdio.h>
#include<conio.h>
#include<math.h>
#include<stdlib.h>
#include<time.h>
#include<windows.h>
#include<string.h>

/*
19:03
20:09

�������� ���������, ��������������� ������ �������� ���������� ��������� ������������� 
������� ������������ �����. ����� �������� ������ � �������� ������������ � ���� ����:

1)	���������� ������
	1.	����� ������
	2.	���������� ���������� �����
2)	����� ���� �����
3)	����� ������
	1.	�� ������� ����������
	2.	�� ���������� ����������
4)	��������� ������
5)	���������� �����
	1.	� ���������� �������
	2.	� ������� �������� �����������
6)	�������� ������ �� ������� �����
	1.	�� ������ ��������
	2.	�� ���������� �����
7)	�����
*/

void generateName(int*, char**);//��������� ����

void trimStr(char*);//������� ������ � ����� ������
void checkName(char*, int);//�������� �����
void checkNumber(int*);//�������� �����
void findName(char**, char*, int, int);//����� �� ����� ��� ��������
void sortName(char **, int, int);//����������

int main()
{
	SetConsoleCP(1251); //��������� ������� ��������� 1251 �� ������� ����� ������
	SetConsoleOutputCP(1251); //��������� ������� ��������� 1251 �� �������� ����� ������
	char ** listName=NULL;
	int size=5;//size=0; ������� �������� ��� ��������
	int i, j, num, switch1, switch2, fExit=1;
	const int MAX_NAME_SIZE=16;
	char tmp[MAX_NAME_SIZE];
	
	listName=(char**)malloc(size*sizeof(char*));
	listName[0]=(char*)malloc(5*sizeof(char));
	strcpy(listName[0], "����");
	
	listName[1]=(char*)malloc(5*sizeof(char));
	strcpy(listName[1], "����");
	
	listName[2]=(char*)malloc(5*sizeof(char));
	strcpy(listName[2], "����");
	
	listName[3]=(char*)malloc(5*sizeof(char));
	strcpy(listName[3], "����");
	
	listName[4]=(char*)malloc(5*sizeof(char));
	strcpy(listName[4], "����");

	do//����� ��������� ����
	{
		printf("1. �������� ���\n");
		printf("2. ����� ���� �����\n");
		printf("3. ����� ������\n");
		printf("4. ��������� ������\n");
		printf("5. ���������� �����\n");
		printf("6. �������� ������ �� ������� �����\n");
		printf("7. �����\n");
		printf("�������� ����� ����:\n");
		printf("---------------------------------------\n");
		scanf("%d", &switch1);
		
		switch(switch1)//������ � ����
		{
			case 1://����������
				{
					printf("�������� ������ ���������� ����:\n");
					printf("1. �������� ���� ���\n");
					printf("2. �������� ��������� ���������� ����\n");
					printf("---------------------------------------\n");
					scanf("%d", &switch2);
				
					switch(switch2)
					{
						case 1://�������� ���� ������
							{
								size++;//����������� ������� ���� �� 1
								listName=(char**)realloc(listName, size*sizeof(char*));//������������������ ������ ��� ������� ����������, ����� ���������� ��� �� 1 �������
								printf("������� ���:\n");
								fflush(stdin);
								fgets(tmp, MAX_NAME_SIZE, stdin);
								trimStr(tmp);
								checkName(tmp, MAX_NAME_SIZE);
								listName[size-1]=(char*)malloc((strlen(tmp)+1)*sizeof(char));//�������� ������, ����������� ���������� ��� �������� ���������� �����
								strcpy(listName[size-1], tmp);//�������� ��������� ��� � ������ ����								
							}
						break;
						
						case 2://�������� ��������� ���������� �����
							{
								printf("������� ���� ��������?\n");
								fflush(stdin);
								scanf("%d", &num);
								checkNumber(&num);
								size+=num;
								listName=(char**)realloc(listName, size*sizeof(char*));
								printf("������� ��� ��� ����������:\n");
								fflush(stdin);
								fgets(tmp, MAX_NAME_SIZE, stdin);
								trimStr(tmp);
								checkName(tmp, MAX_NAME_SIZE);
								for(i=size-num; i<size; i++)
								{
									listName[i]=(char*)malloc((strlen(tmp)+1)*sizeof(char));
									strcpy(listName[i], tmp);
								}
							}
						break;
												
						default://��������� � �������� ������ �������
							{
								printf("������������ ����� ����� ����!\n");
							}
						break;						
					}
				}
			break;
			
			
			
			case 2://����� ���� �����
				{
					printf("������ ����:\n");
					for(i=0; i<size; i++) {
						printf("%d) %s\n", i+1, listName[i]);
					}
				}
			break;
			
			
			
			case 3://�����
				{
					printf("����� �����:\n");
					printf("1. �� ������� ����������\n");
					printf("2. �� ���������� ����������\n");//strstr
					printf("---------------------------------------\n");
					scanf("%d", &switch2);
				
					switch(switch2)
					{
						case 1://�� ������� ����������
							{
								printf("������� ��� ��� ������:\n");
								fflush(stdin);
								fgets(tmp, MAX_NAME_SIZE, stdin);
								trimStr(tmp);
								findName(listName, tmp, size, switch2);
							}
						break;
						
						case 2://�� ���������� ����������
							{
								printf("������� ����� ��� ������:\n");
								fflush(stdin);
								fgets(tmp, MAX_NAME_SIZE, stdin);
								trimStr(tmp);
								findName(listName, tmp, size, switch2);
							}
						break;
												
						default://��������� � �������� ������ �������
							{
								printf("������������ ����� ����� ����!\n");
							}
						break;
					}
				}
			break;
			
			
			
			case 4://��������� �����
				{
					int num;
					printf("�������� ����� ����� ��� ���������:\n");//�������������� ������� ������ ����
					fflush(stdin);
					scanf("%d", &num);
					checkNumber(&num);
					printf("�� ������ �������� ���: %s\n", listName[num-1]);
					printf("������� ����� ���:\n");
					fflush(stdin);
					fgets(tmp, MAX_NAME_SIZE, stdin);
					trimStr(tmp);
					checkName(tmp, MAX_NAME_SIZE);
					free(listName[num-1]);
					listName[num-1]=(char*)malloc((strlen(tmp)+1)*sizeof(char));
					strcpy(listName[num-1], tmp);
				}
			break;
			
			
			
			case 5://����������
				{
					printf("���������� ����:\n");
					printf("1. � ���������� �������\n");
					printf("2. � �������� �����������\n");
					printf("---------------------------------------\n");
					scanf("%d", &switch2);
				
					switch(switch2)
					{
						case 1://� ���������� �������
							{
								sortName(listName, size, switch2);
							}
						break;
						
						case 2://� �������� �����������
							{
								sortName(listName, size, switch2);
							}
						break;
												
						default://��������� � �������� ������ �������
							{
								printf("������������ ����� ����� ����!\n");
							}
						break;
					}
				}
			break;
			
				
			case 6://��������
				{
					printf("�������� ������ �� ������� �����:\n");
					printf("1. �� ������ ��������\n");
					printf("2. �� ���������� �����\n");
					printf("---------------------------------------\n");
					scanf("%d", &switch2);
				
					switch(switch2)
					{
						case 1://�� ������ ��������
							{
								printf("������� ����� ����� ��� ��������:\n");
								fflush(stdin);
								scanf("%d", &num);
								checkNumber(&num);
								free(listName[num-1]);
								for(i=num-1; i<size-1; i++) {
									listName[i]=listName[i+1];
								}
								size--;
								listName=(char**)realloc(listName, size*sizeof(char*));
							}
						break;
						
						case 2://�� ���������� �����
							{
								printf("������� ��� ��� ��������:\n");
								fflush(stdin);
								fgets(tmp, MAX_NAME_SIZE, stdin);
								trimStr(tmp);
								int counter=0;
								for(i=0; i<size; i++) {
									if(strcmp(listName[i], tmp)==0) {
										free(listName[i]);
										for(j=i; j<size-1; j++) {
												listName[j]=listName[j+1];
											}
										size--;
										listName=(char**)realloc(listName, size*sizeof(char*));
										counter++;
									}
								}
								if(counter==0) {
									printf("���������� ���\n");
								} else {
									printf("������� ����: %d\n", counter);
								}
							}
						break;
												
						default://��������� � �������� ������ �������
							{
								printf("������������ ����� ����� ����!\n");
							}
						break;
					}
				}
			break;
			
			case 7://������� ��� ������� ���������� ������
				{
					fExit=0;
					for(i=0; i<size; i++)
					{
						free(listName[i]);
					}
					free(listName);
					listName=NULL;
				}
			break;
	
			default://��������� � �������� ������ ����
				{
					printf("������������ ����� ����� ����!\n");
				}
			break;
		}
	
	printf("\n������� ����� �������\n");
	getch();
		
	system("cls");//�������� ���� ������� ����� ������� ������ ����
	fflush(stdin);//�������� ������� �����
		
	}while(fExit==1);//��. ����� ���� 7
	
	printf("������ ��������� ���������!\n");	
	getch();
	return 0;
}



//������� ������� ������ � ����� ������
void trimStr(char * str) {
	if(str[strlen(str)-1]=='\n') {
		str[strlen(str)-1]='\0';
	}
}

//������ �������� ����� �����
void checkName(char * tmp, int MAX_NAME_SIZE) {
	int i, check=0, counter=0;
	while(check==0) {
		for(i=0; i<strlen(tmp); i++) {
			if((tmp[i]>='A' && tmp[i]<='z') || (tmp[i]>='�' && tmp[i]<='�') || tmp[i]==" ") {
				counter++;
			}			
		}
		
		if(counter==strlen(tmp)) {
			check=1;
		} else {
			printf("\n��������� ������������ ���������� ����� � ���������� ��� ���:\n");
			fflush(stdin);
			fgets(tmp, MAX_NAME_SIZE, stdin);//������ ������
			trimStr(tmp);//������� ����� � �����
		}
	}
}

//������� �������� �����
void checkNumber(int * num) {
	int tmp;
	tmp=*num;
	while(tmp<1 || tmp>100) {
		printf("������������ ����! ���������� ��� ���:\n");
		fflush(stdin);
		scanf("%d", &tmp);
	}
	*num=tmp;
}

//������� ������
void findName(char ** listName, char * tmp, int size, int switch2) {
	int i, counter=0;
	if(switch2==1) {
		for(i=0; i<size; i++) {
			if(strcmp(listName[i], tmp)==0) {
				counter++;
				printf("%d) %s\n", i+1, listName[i]);
			}
		}
		if(counter==0) {
			printf("���������� ���\n");
		} else {
			printf("������� ����������: %d\n", counter);
		}
	}
	if(switch2==2) {
		for(i=0; i<size; i++) {
			if(strstr(listName[i], tmp)!=NULL) {
				counter++;
				printf("%d) %s\n", i+1, listName[i]);
			}
		}
		if(counter==0) {
			printf("���������� ���\n");
		} else {
			printf("������� ����������: %d\n", counter);
		}
	}	
}

//������� ����������
void sortName(char ** listName, int size, int switch2) {
	int i, j;
	char * tmp;
	if(switch2==1) {
		for(j=0; j<size-1; j++)	{
			for(i=0; i<size-1-j; i++) {
				if(strcmp(listName[i], listName[i+1])>0) {
					tmp=listName[i];
					listName[i]=listName[i+1];
					listName[i+1]=tmp;
				}
			}
		}
	}
	
	if(switch2==2) {
		for(j=0; j<size-1; j++)	{
			for(i=0; i<size-1-j; i++) {
				if(strcmp(listName[i], listName[i+1])<0) {
					tmp=listName[i];
					listName[i]=listName[i+1];
					listName[i+1]=tmp;
				}
			}
		}
	}
	tmp=NULL;
}
