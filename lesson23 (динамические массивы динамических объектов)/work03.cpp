#include<stdio.h>
#include<conio.h>
#include<math.h>
#include<stdlib.h>
#include<time.h>
#include<windows.h>
#include<string.h>

//������������ ������ ������

class Human {
	private:
		char * name;
		int age;
		double salary;
	public:
		Human(const char * name, int age, double salary){
			//this->name=(char*)malloc((strlen(name)+1)*sizeof(char));
			this->name=new char[strlen(name)+1];
			strcpy(this->name, name);
			this->age=age;
			this->salary=salary;
			printf("�������� ����������� � �����������\n");
		}
		
		Human(){
			//this->name=(char*)malloc(sizeof(char));
			this->name=new char[1];//������������, � �� �������
			this->name[0]='\0';
			this->age=0;
			this->salary=0;
			printf("�������� ����������� ��� ����������\n");
		}
		
		void setName(char * name){
			if(this->name!=NULL){
				//free(this->name);
				delete[] this->name;
				this->name=NULL;
			}
			//this->name=(char*)malloc((strlen(name)+1)*sizeof(char));
			this->name=new char[strlen(name)+1];
			strcpy(this->name, name);
		}
		
		char * getName(){
			return this->name;
		}
		
		void setAge(int age){
			this->age=age;
		}
		
		int getAge(){
			return this->age;
		}
		
		void setSalary(double salary){
			this->salary=salary;
		}
		
		double getSalary(){
			return this->salary;
		}
		
		void info(){
			printf("���: %s, �������: %d, ��������: %.2lf\n", this->name, this->age, this->salary);
		}
		
		~Human(){
			if(this->name!=NULL){
				delete[] this->name;
				//delete ��������� - ��� ������ ��������
				//delete [] ��������� - ��� �������
			}
			printf("�������� ����������\n");
		}
};

void trimStr(char*);

int main()
{
	SetConsoleCP(1251); //��������� ������� ��������� 1251 �� ������� ����� ������
	SetConsoleOutputCP(1251); //��������� ������� ��������� 1251 �� �������� ����� ������
	Human * human1;
	human1=new Human();//�������� ������, ����������� ��� �������� ������� Human ��� ����������
	human1->info();
	
	delete human1;
	human1=NULL;
	
	human1=new Human("����", 29, 450);//�.�. human1 ���������, �� ����� ������������ ������ � ���� ����� �������� ����� ���������
	human1->info();
	
	const int MAX_NAME_SIZE=16;
	char tmpName[MAX_NAME_SIZE];
	int tmpAge;
	double tmpSalary;
	printf("������� ���:\n");
	fflush(stdin);
	fgets(tmpName, MAX_NAME_SIZE, stdin);
	trimStr(tmpName);
	printf("������� �������:\n");
	scanf("%d", &tmpAge);
	printf("������� ��������:\n");
	scanf("%lf", &tmpSalary);
	
	Human * human2=new Human(tmpName, tmpAge, tmpSalary);
	human2->info();
	
	delete human1;
	human1=NULL;
	
	delete human2;
	human2=NULL;
	
	getch();
	return 0;
}

void trimStr(char * str)
{
	if(str[strlen(str)-1]=='\n')
		str[strlen(str)-1]='\0';
}
