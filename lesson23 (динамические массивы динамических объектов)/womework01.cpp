#include<stdio.h>
#include<conio.h>
#include<math.h>
#include<stdlib.h>
#include<time.h>
#include<windows.h>
#include<string.h>

class Student {
	private:
		char * firstName;
		char * lastName;
		int birthday;
		int monthOfBirth;
		int yearOfBirth;
		char * city;
		int phone;
		char * faculty;
		char * course;
		
	public:
		Student(const char * firstName, const char * lastName, int birthday, int monthOfBirth, int yearOfBirth, const char * city, int phone, const char * faculty, const char * course){
			this->firstName=new char[strlen(firstName)+1];
			strcpy(this->firstName, firstName);
			
			this->lastName=new char[strlen(lastName)+1];
			strcpy(this->lastName, lastName);
			
			this->birthday=birthday;
			this->monthOfBirth=monthOfBirth;
			this->yearOfBirth=yearOfBirth;
			
			this->city=new char[strlen(city)+1];
			strcpy(this->city, city);
			
			this->phone=phone;
			
			this->faculty=new char[strlen(faculty)+1];
			strcpy(this->faculty, faculty);
			
			this->course=new char[strlen(course)+1];
			strcpy(this->course, course);
			
			//printf("�������� ����������� � �����������\n");
		}
		
		Student(){
			this->firstName=NULL;
			this->lastName=NULL;
			this->birthday=NULL;
			this->monthOfBirth=NULL;
			this->yearOfBirth=NULL;
			this->city=NULL;
			this->phone=NULL;
			this->faculty=NULL;
			this->course=NULL;
			
			//printf("�������� ����������� ��� ����������\n");
		}
		
		void setFirstName(char * firstName){
			if(this->firstName!=NULL){
				delete[] this->firstName;
				this->firstName=NULL;
			}
			this->firstName=new char[strlen(firstName)+1];
			strcpy(this->firstName, firstName);
		}
		
		char * getFirstName(){
			return this->firstName;
		}
		
		void setLastName(char * lastName){
			if(this->lastName!=NULL){
				delete[] this->lastName;
				this->lastName=NULL;
			}
			this->lastName=new char[strlen(lastName)+1];
			strcpy(this->lastName, lastName);
		}
		
		char * getLastName(){
			return this->lastName;
		}
		
		void setBirthday(int birthday){
			if(birthday<1 || birthday>31){
				printf("�������������� ����. �����, ���� �������� ����� - 13-�� �����!\n");
				this->birthday=13;
			} else {
				this->birthday=birthday;
			}
		}
		
		int getBirthday(){
			return this->birthday;
		}
		
		void setMonthOfBirth(int monthOfBirth){
			if(monthOfBirth<1 || monthOfBirth>12){
				printf("��� ��� ����� ����� �� �����? �������� ���� ������!\n");
				this->monthOfBirth=1;
			} else {
				this->monthOfBirth=monthOfBirth;
			}
		}
		
		int getMonthOfBirth(){
			return this->monthOfBirth;
		}
		
		void setYearOfBirth(int yearOfBirth){
			if(yearOfBirth<1900 || yearOfBirth>2021){
				printf("���-�� ������ � ���� �������? ���� 7 ��� - ������� ����� �����!\n");
				this->yearOfBirth=2005;
			} else {
				this->yearOfBirth=yearOfBirth;
			}
		}
		
		int getYearOfBirth(){
			return this->yearOfBirth;
		}
		
		void setCity(char * city){
			if(this->city!=NULL){
				delete[] this->city;
				this->city=NULL;
			}
			this->city=new char[strlen(city)+1];
			strcpy(this->city, city);
		}
		
		char * getCity(){
			return this->city;
		}
		
		void setPhone(int phone){
			if(phone<1){
				printf("������������� �����? �� �����...\n");
			}
			this->phone=phone;
		}
		
		int getPhone(){
			return this->phone;
		}
		
		void setFaculty(char * faculty){
			if(this->faculty!=NULL){
				delete[] this->faculty;
				this->faculty=NULL;
			}
			this->faculty=new char[strlen(faculty)+1];
			strcpy(this->faculty, faculty);
		}
		
		char * getFaculty(){
			return this->faculty;
		}
		
		void setCourse(char * course){
			if(this->course!=NULL){
				delete[] this->course;
				this->course=NULL;
			}
			this->course=new char[strlen(course)+1];
			strcpy(this->course, course);
		}
		
		char * getCourse(){
			return this->course;
		}
		
		void info(){
			printf("%s %s, 	%.2d-%.2d-%d�.�; 	�.%s, 	���.%d, 	%s, 	%s\n", this->firstName, this->lastName, this->birthday, this->monthOfBirth, this->yearOfBirth, this->city, this->phone, this->faculty, this->course);
		}
		
		~Student(){
			if(this->firstName!=NULL){
				delete[] this->firstName;
			}
			
			if(this->lastName!=NULL){
				delete[] this->lastName;
			}
			
			if(this->city!=NULL){
				delete[] this->city;
			}
			
			if(this->faculty!=NULL){
				delete[] this->faculty;
			}
			
			if(this->course!=NULL){
				delete[] this->course;
			}
			
			//printf("�������� ����������\n");
		}
};

void trimString(char*);

int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	
	Student ** students=NULL;
	
	int i, j=0, size=NULL;
	const int MAX_NAME_SIZE=16;
	char tmp[MAX_NAME_SIZE];
	
	printf("������������� ���� ���������? (1 - ��, ����� ������ - ���)\n");
	scanf("%d", &i);
	if(i==1){
		size=3;
		students = new Student * [size];
		
		students[0]=new Student("����", "��������", 9, 2, 1980, "������", 1234567, "������������������", "��������");
		students[1]=new Student("����", "������", 1, 12, 2000, "������", 7654321, "������ ��������������", "��������");
		students[2]=new Student("����", "�������", 31, 5, 1990, "�����", 7775566, "�����������", "���������");
		
		printf("���� ��������� ����� ��������:\n");
		for(i=0; i<size; i++){
			students[i]->info();
		}
	}
	
	printf("������� ��� ��������� ��������:\n");
	scanf("%d", &j);
	while(j<1){
		printf("������������ ����\n");
		scanf("%d", &j);
	}
	
	////////////////////////////////////////////////////////
	size+=j;
	students = new Student * [size];
	////////////////////////////////////////////////////////
	
	for(i=size-j; i<size; i++){
		printf("������� ���� � %d ��������\n", i+1);
		
		students[i]=new Student();
			
		printf("������� ���:\n");
		fflush(stdin);
		fgets(tmp, MAX_NAME_SIZE, stdin);
		trimString(tmp);
		students[i]->setFirstName(tmp);
		
		printf("������� �������:\n");
		fflush(stdin);
		fgets(tmp, MAX_NAME_SIZE, stdin);
		trimString(tmp);
		students[i]->setLastName(tmp);
		
		printf("������� ���� ��������:\n");
		fflush(stdin);
		scanf("%d", &j);
		students[i]->setBirthday(j);
		
		printf("������� ����� ��������:\n");
		fflush(stdin);
		scanf("%d", &j);
		students[i]->setMonthOfBirth(j);
		
		printf("������� ��� ��������:\n");
		fflush(stdin);
		scanf("%d", &j);
		students[i]->setYearOfBirth(j);
		
		printf("������� �����:\n");
		fflush(stdin);
		fgets(tmp, MAX_NAME_SIZE, stdin);
		trimString(tmp);
		students[i]->setCity(tmp);
		
		printf("������� ����� ��������:\n");
		fflush(stdin);
		scanf("%d", &j);
		students[i]->setPhone(i);
		
		printf("������� ���������:\n");
		fflush(stdin);
		fgets(tmp, MAX_NAME_SIZE, stdin);
		trimString(tmp);
		students[i]->setFaculty(tmp);
		
		printf("������� ������������� (����� ���������, ���� �������):\n");
		fflush(stdin);
		fgets(tmp, MAX_NAME_SIZE, stdin);
		trimString(tmp);
		students[i]->setCourse(tmp);
	}
	
	for(int i=0; i<size; i++){
		students[i]->info();
	}
	
	for(int i=0; i<size; i++){
		delete students[i];
		students[i]=NULL;
	}
	
	delete[] students;
	students=NULL;
	
	getch();
	return 0;
}

void trimString(char * str)
{
	if(str[strlen(str)-1]=='\n')
		str[strlen(str)-1]='\0';
}
