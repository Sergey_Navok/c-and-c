#include<stdio.h>
#include<conio.h>
#include<math.h>
#include<stdlib.h>
#include<time.h>
#include<windows.h>
#include<string.h>

class Human {
	private:
		char * name;
		int age;
		double salary;
	public:
		Human(char * name, int age, double salary){
			this->name=(char*)malloc((strlen(name)+1)*sizeof(char));
			strcpy(this->name, name);
			this->age=age;
			this->salary=salary;
		}
		
		Human(){
			this->name=(char*)malloc(sizeof(char));
			this->name[0]='\0';
			this->age=0;
			this->salary=0;
		}
		
		void setName(char * name){
			if(this->name!=NULL){
				free(this->name);
				this->name=NULL;
			}
			this->name=(char*)malloc((strlen(name)+1)*sizeof(char));
			strcpy(this->name, name);
		}
		
		char * getName(){
			return this->name;
		}
		
		void setAge(int age){
			this->age=age;
		}
		
		int getAge(){
			return this->age;
		}
		
		void setSalary(double salary){
			this->salary=salary;
		}
		
		double getSalary(){
			return this->salary;
		}
		
		void info(){
			printf("���: %s, �������: %d, ��������: %.2lf\n", this->name, this->age, this->salary);
		}
		
		~Human(){
			if(this->name!=NULL){
				free(this->name);
			}
		}
};

int main()
{
	SetConsoleCP(1251); //��������� ������� ��������� 1251 �� ������� ����� ������
	SetConsoleOutputCP(1251); //��������� ������� ��������� 1251 �� �������� ����� ������
	
	
	
	printf("�������:\n");
	printf("�������: \n");
	
	
	
	getch();
	return 0;
}
