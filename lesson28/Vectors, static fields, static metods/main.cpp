#include<iostream>
#include<Windows.h>
#include<string>
#include<vector>
#include<algorithm>

using namespace std;

class Pets {
private:
	static string owner; //����������� static
	static int petsCount;
	string name;
	double price;
public:
	Pets(string name) {
		this->name = name;
		Pets::petsCount++;
	}

	Pets(string name, double price) {
		this->name = name;
		this->price = price;
		Pets::petsCount++;
	}

	Pets() {
		this->name = "";
		this->price = 0;
		Pets::petsCount++;
	}

	static void setOwner(string newOwner) {
		Pets::owner = newOwner;
	}

	static string getOwner() {
		return Pets::owner;
	}

	static int getPetsCount() {
		return Pets::petsCount;
	}

	void setName(string name) {
		this->name = name;
		this->name.shrink_to_fit();
	}

	string getName() {
		return this->name;
	}

	void setPrice(double price) {
		this->price = price;
	}

	double getPrice() {
		return this->price;
	}

	virtual void voise() = 0; //������ ������������ �����, �������� ������� ������������ ������. �����, ������� �� �������� ����������.

	virtual void info() = 0;

	friend bool petsCmpNameUp(Pets*, Pets*);

	virtual ~Pets() {
		Pets::petsCount--;
	}
};

string Pets::owner = "����";
int Pets::petsCount = 0;

bool petsCmpNameUp(Pets* pet1, Pets* pet2) {
	if (pet1->name > pet2->name) {
		return false;
	}
	else {
		return true;
	}
}

class Dogs :public Pets {
public:
	Dogs(string name) :Pets(name) {

	}

	Dogs(string name, double price) :Pets(name, price) {

	}

	Dogs() :Pets() {

	}

	void voise() {
		cout << "���-���" << endl;
	}

	void info() {
		cout << "����� �� ������ " << this->getName() << ", ���������: " << this->getPrice() << endl;
	}

	virtual ~Dogs() {

	}
};

class Cats :public Pets {
public:
	Cats(string name) :Pets(name) {

	}

	Cats(string name, double price) :Pets(name, price) {

	}

	Cats() :Pets() {

	}

	void voise() {
		cout << "���-���" << endl;
	}

	void info() {
		cout << "����� �� ������ " << this->getName() << ", ���������: " << this->getPrice() << endl;
	}

	virtual ~Cats() {

	}
};

int main() {
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	/*
	//Pets* pets = new Pets(); //�������� ������� ����������� ������� ����������
	Pets** pets = NULL;
	int size = 5;

	pets = new Pets * [size]; //������������ ��������� ������ ��� �������, ���������� �� size ���������� ��� ������� ���� Pets
	pets[0] = new Dogs("����");
	pets[1] = new Cats("�����");
	pets[2] = new Cats("�����");
	pets[3] = new Dogs("�����");
	pets[4] = new Cats("������");

	for (int i = 0; i < size; i++) {
		cout << i + 1 << ")";
		pets[i]->info();
		pets[i]->voise();
	}

	for (int i = 0; i < size; i++) {
		delete pets[i];
	}

	delete[] pets;
	pets = NULL;
	*/

	//����������
	//�����
	//��������
	//����������

	vector <Pets*> pets;
	pets.push_back(new Cats("������", 100));
	pets.push_back(new Dogs("����", 500));
	pets.push_back(new Cats("������", 120));
	pets.push_back(new Dogs("������", 450));

	cout << "����������: " << pets.size() << ". ������ : " << pets.capacity() << endl;

	string name;
	double price;

	Pets* pet = new Cats("������", 200);

	int fExit = 1, sw1, sw2;

	do {
		cout << "�������� ����� ����:" << endl;
		cout << "1. ����������" << endl;
		cout << "2. ����������" << endl;
		cout << "3. ��������" << endl;
		cout << "4. ����������" << endl;
		cout << "5. �����" << endl;

		//cin.ignore(INT_MAX, '\n');
		cin >> sw1;

		switch (sw1) {
			case 1: {
				cout << "�������� ��� ���������" << endl;
				cout << "1. ���" << endl;
				cout << "2. ������" << endl;
				cin >> sw2;
				cout << "������� ������ �������: " << endl;

				//cin.ignore(INT_MAX, '\n');
				while (cin.get() != '\n');

				getline(cin, name);
				cout << "������� ���������: " << endl;
				cin >> price;

				switch (sw2) {
					case 1: {
						pets.push_back(new Cats(name, price));
					} break;

					case 2: {
						pets.push_back(new Dogs(name, price));
					} break;

					default: {
						cout << "������������ ����� ����!" << endl;
					}
				}
			} break;
			
			case 2: {
				cout << "������: " << Pets::getOwner() << endl;
				cout << "����� ���������� ��������: " << Pets::getPetsCount() << endl;
				cout << "���������� : " << endl;
				for (int i = 0; i < pets.size() ; i++) {
					cout << i + 1 << ") ";
					pets[i]->info();
					pets[i]->voise();
				}
			} break;

			case 3: {
				int num;
				cout << "�������� ������� ��� ��������" << endl;
				cin >> num;
				while(num<1 || num>pets.size()) {
					cout << "������������ ����" << endl;
					cin >> num;
				}
				delete pets[num - 1];
				pets.erase(pets.begin() + num -1);
				pets.shrink_to_fit();

			} break;

			case 4: {
				sort(pets.begin(), pets.end(), petsCmpNameUp);
				Pets::setOwner("����");
			} break;

			case 5: {
				fExit = 0;
				for (int i = 0; i < pets.size(); i++) {
					delete pets[i];
				}
				pets.clear(); //������� ��� �������� vector
				pets.shrink_to_fit();
			} break;

			default: {
				cout << "������������ ����� ����!" << endl;
			}
		}

		system("pause");
		system("cls");

	} while (fExit == 1);
	
	delete pet;


	system("pause");
	return 0;
}