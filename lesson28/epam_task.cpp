#include<iostream>
#include<Windows.h>
#include<string>
#include<vector>
#include<algorithm>

using namespace std;

class Client {
private:
	static double tariff;
	string name;
	int energy;
	string type;
public:
	Client(string name, int energy) {
		this->name = name;
		//this->name.shrink_to_fit();
		this->energy = energy;
	}
	
	Client() {
		this->name = "";
		this->energy = 0;
	}
	
	static void setTariff(double newTariff) {
		Client::tariff = newTariff;
	}
	
	static double getTariff() {
		return Client::tariff;
	}
	
	void setName(string name) {
		this->name = name;
		//this->name.shrink_to_fit();
	}
	
	string getName() {
		return this->name;
	}
	
	void setEnergy(int energy) {
		this->energy = energy;
	}
	
	int getEnergy() {
		return this->energy;
	}
	
	void setType(string type) {
		this->type = type;
	}
	
	string getType() {
		return this->type;
	}
	
	virtual double payment() {
	
	}
	
	friend bool compareEnergyUp(Client*, Client*);
	friend bool comparePaymentUp(Client*, Client*);
	friend bool compareClientType(Client*, Client*);
	
	virtual ~Client() {
		
	}
};

double Client::tariff = 0.15;

bool compareEnergyUp(Client* client1, Client* client2) {
	if (client1->getEnergy() > client2->getEnergy()) {
		return false;
	}
	else {
		return true;
	}
}

bool comparePaymentUp(Client* client1, Client* client2) {
	if (client1->payment() > client2->payment()) {
		return false;
	}
	else {
		return true;
	}
}

bool compareClientType(Client* client1, Client* client2) {
	if (client1->getType() > client2->getType()) {
		return false;
	}
	else {
		return true;
	}
}

//-----------------------------------------//

class Ordinary :public Client {
public:
	Ordinary(string name, int energy) :Client (name, energy) {
		this->setType("1 - �������");
	}
	
	Ordinary() :Client() {
		
	}
	
	double payment() {
		double x;
		x = this->getEnergy() * Client::getTariff();
		return x;
	}
	
	~Ordinary() {
		
	}
};

//-----------------------------------------//

class Limit :public Client {
private:
	static double limitCoeficient;
public:
	Limit(string name, int energy) :Client (name, energy) {
		this->setType("2 - � �������");
	}
	
	Limit() :Client() {
		
	}
	
	static void setLimitCoeficient(double newLimitCoeficient) {
		Limit::limitCoeficient = newLimitCoeficient;
	}
	
	static double getLimitCoeficient() {
		return Limit::limitCoeficient;
	}
	
	double payment() {
		double x;
		x = this->getEnergy();
		if(x < 250) {
			x = x * Client::getTariff();
			return x;
		} else {
			double y, z;
			y = x - 250;
			x = x * Client::getTariff();
			y = y * Client::getTariff() * (1 + Limit::limitCoeficient);
			z = x + y;
			return z;
		}
	}
	
	~Limit() {
		
	}
};

double Limit::limitCoeficient = 1/3;

//-----------------------------------------//

class Privilege :public Client {
private:
	static double privilegeCoeficient;
public:
	Privilege(string name, int energy) :Client (name, energy) {
		this->setType("3 - ��������");
	}
	
	Privilege() :Client() {
		
	}
	
	static void setPrivilegeCoeficient(double newPrivilegeCoeficient) {
		Privilege::privilegeCoeficient = newPrivilegeCoeficient;
	}
	
	static double getTariff() {
		return Privilege::privilegeCoeficient;
	}
	
	double payment() {
		double x;
		x = this->getEnergy() * Privilege::privilegeCoeficient;
		return x;
	}
	
	~Privilege() {
		
	}
};

double Privilege::privilegeCoeficient = Client::getTariff() * 2/3;

//-----------------------------------------//

class Heating :public Client {
private:
	static double heatingCoeficient;
public:
	Heating(string name, int energy) :Client (name, energy) {
		this->setType("4 - ���������");
	}
	
	Heating() :Client() {
		
	}
	
	static void setHeatingCoeficient(double newHeatingCoeficient) {
		Heating::heatingCoeficient = newHeatingCoeficient;
	}
	
	static double getTariff() {
		return Heating::heatingCoeficient;
	}
	
	double payment() {
		double x;
		x = this->getEnergy() * Heating::heatingCoeficient;
		return x;
	}
	
	~Heating() {
		
	}
};

double Heating::heatingCoeficient = Client::getTariff() * 1/15;

//-----------------------------------------//

int main() {
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	
	//----------����������� ��������� ������� � ����-���----------//
	Client::setTariff(0.15);
	Limit::setLimitCoeficient(1/3);
	Privilege::setPrivilegeCoeficient(Client::getTariff() * 2/3);
	Heating::setHeatingCoeficient(Client::getTariff() * 1/15);
	
	vector <Client*> clients;
	clients.push_back(new Ordinary("����", 10));
	clients.push_back(new Ordinary("����", 150));
	clients.push_back(new Ordinary("����", 300));
	clients.push_back(new Limit("����", 11));
	clients.push_back(new Limit("����", 151));
	clients.push_back(new Limit("����", 301));
	clients.push_back(new Privilege("��������", 12));
	clients.push_back(new Privilege("���������", 152));
	clients.push_back(new Privilege("������", 300));
	clients.push_back(new Heating("�����", 13));
	clients.push_back(new Heating("���", 151));
	clients.push_back(new Heating("������", 303));
	
	//-----------------------------------------//
	cout << "����� ���� ��������:" << endl;
	for (int i = 0; i < clients.size() ; i++) {
		cout << i + 1 << ") ������: " << clients[i]->getName() << ", ���������� ��������: " << clients[i]->getEnergy() << "���" << endl;
	}
	system("pause");
	
	//-----------------------------------------//
	cout << "\n���������� �� �����������:" << endl;
	sort(clients.begin(), clients.end(), compareEnergyUp);
	for (int i = 0; i < clients.size() ; i++) {
		cout << i + 1 << ") ������: " << clients[i]->getName() << ", ���������� ��������: " << clients[i]->getEnergy() << "���" << endl;
	}
	system("pause");
	
	//-----------------------------------------//
	cout << "\n���������� �� ����� ������:" << endl;
	sort(clients.begin(), clients.end(), comparePaymentUp);
	for (int i = 0; i < clients.size() ; i++) {
		cout << i + 1 << ") ������: " << clients[i]->getName() << ", ����� ������: " << clients[i]->payment() << "���." << endl;
	}
	system("pause");
	
	//-----------------------------------------//
	cout << "\n���������� ������� �� ���� ��������:" << endl;
	sort(clients.begin(), clients.end(), compareClientType);
	for (int i = 0; i < clients.size() ; i++) {
		cout << i + 1 << ") ������: " << clients[i]->getName() << ", " << clients[i]->getType() << endl;
	}
	system("pause");
	
	//-----------------------------------------//
	cout << "\n������� ����� ������" << endl;
	double sum0 = 0;
	for (int i = 0; i < clients.size() ; i++) {
		sum0 += clients[i]->payment();
	}
	cout << "����� �����: " << sum0 << "���." << endl;
	system("pause");
	
	//-----------------------------------------//
	cout << "\n������� �������� �����" << endl;
	double sum1 = 0;
	for (int i = 0; i < clients.size() ; i++) {
		sum1 += clients[i]->getEnergy() * Client::getTariff();
	}
	cout << "����� ����� ��� �����: " << sum1 << "���." << endl;
	cout << "����� ����� ������: " << sum1-sum0 << "���." << endl;
	
	//---------------��������---------------//
	for (int i = 0; i < clients.size(); i++) {
		delete clients[i];
	}
	clients.clear(); //������� ��� �������� vector
	//clients.shrink_to_fit();
	
	system("pause");
	return 0;
}
