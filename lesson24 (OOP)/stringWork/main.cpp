#include<iostream>
#include<Windows.h>
#include<string>//��������� �������� ��������� ��������

using namespace std;

void showStr(string*);

int main() {
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);

	string str;//��������� - ����������� ����� �++
	cout << "������: " << str << ", ������: " << str.size() << ", ������: " << str.capacity() << endl;
	str = "12345";
	cout << "������: " << str << ", ������: " << str.size() << ", ������: " << str.capacity() << endl;
	str = str + "1234567890";
	cout << "������: " << str << ", ������: " << str.size() << ", ������: " << str.capacity() << endl;
	str = str + '!';
	cout << "������: " << str << ", ������: " << str.size() << ", ������: " << str.capacity() << endl;
	str.resize(48, '+');
	cout << "������: " << str << ", ������: " << str.size() << ", ������: " << str.capacity() << endl;
	str.resize(8);
	cout << "������: " << str << ", ������: " << str.size() << ", ������: " << str.capacity() << endl;
	showStr(&str);
	str.shrink_to_fit();//��������� ������
	showStr(&str);
	str.clear();//�������� ������
	showStr(&str);
	if (str.empty() == true) {
		cout << "������ �����" << endl;
	}
	string str1, str2;
	cout << "������� ������ ������" << endl;
	//cin >> str1;//���������� �� ������� ����������� �������
	getline(cin, str1);
	cout << str1;
	cout << "������� ������ ������" << endl;
	getline(cin, str2);
	if (str1 == str2) {
		cout << "������ ����������" << endl;
	}
	else {
		cout << "������ ��������" << endl;
	}
	//cout << str1.find(str2) << endl;//������ ����������, �� ���� �� �������, �� ������ ���� ���� ������������ int
	if (str1.find(str2) < str1.size()) {
		cout << "��������� � ������ �������" << endl;
	}
	else {
		cout << "��������� � ������ �� �������" << endl;
	}



	system("pause");
	return 0;
}

void showStr(string * _str) {//string _str=str; ���� ���������� �� �����, �� ��������� ����� ������, � �� �������� � �������
	cout << "������: " << *_str << ", ������: " << _str->size() << ", ������: " << _str->capacity() << endl;
}