#include<stdio.h>
#include<conio.h>
#include<locale.h>
#include<math.h>
#include<stdlib.h>
#include<time.h>

main()
{
	setlocale(LC_ALL, "rus");
	const int MAX_SIZE=50;//������������ ������
	int a[MAX_SIZE];//���������� �������
	int i, size;
	printf("������� ���������� ��������� [1...%d]:\n", MAX_SIZE);
	scanf("%d", &size);//���� ������������� ���������� ���������
	
	while(size<1 || size>MAX_SIZE)//�������� ���������
	{
		printf("����������� ����\n");
		scanf("%d", &size);
	}
	
	//���������� ������� ���������� ������� [-50...50]
	srand(time(0));
	for(i=0; i<size; i++)
	{
		a[i]=rand()%101-50;//rand()%(50-(-50+1)+(-50));
	}
	
	//����� �������
	printf("�������� ������\n");
	for(i=0; i<size; i++)
	{
		printf("%d ", a[i]);
	}
	printf("\n");
	
/*
1. ��� ������. ��������� ���������:
�) ������� �������� ��������������� ���� ����� ��������� �������. 
*/
	
	int num1, num2;
	printf("������� ����� ������� ��������:\n");
	scanf("%d", &num1);
	while(num1<1 ||num1>size)
	{
		printf("������������ ����\n");
		scanf("%d", num1);		
	}
	
	printf("������� ����� ������� ��������:\n");
	scanf("%d", &num2);
	while(num2<1 ||num2>size || num2==num1)
	{
		printf("������������ ����\n");
		scanf("%d", num2);		
	}
	
	double avg;
	avg=(a[num1-1]+a[num2-1])/2.0;
	
	printf("������� �������������� = %.2lf\n", avg);
	

	getch();
}
