#include<stdio.h>
#include<conio.h>
#include<locale.h>
#include<math.h>
#include<stdlib.h>
#include<time.h>

main()
{
	setlocale(LC_ALL, "rus");
	const int MAX_SIZE=50;//������������ ������
	int a[MAX_SIZE];//���������� �������
	int i, size;
	printf("������� ���������� ��������� [1...%d]:\n", MAX_SIZE);
	scanf("%d", &size);//���� ������������� ���������� ���������
	
	while(size<1 || size>MAX_SIZE)//�������� ���������
	{
		printf("����������� ����\n");
		scanf("%d", &size);
	}
	
	//���������� ������� ���������� ������� [-50...50]
	srand(time(0));
	for(i=0; i<size; i++)
	{
		a[i]=rand()%101-50;//rand()%(50-(-50+1)+(-50));
	}
	
	//����� �������
	printf("�������� ������\n");
	for(i=0; i<size; i++)
	{
		printf("%d ", a[i]);
	}
	printf("\n");
	
/*
1. ��� ������. ��������� ���������: 
�) ������� ����������� ����� �� ������ �������� �������; 
*/

	int num;
	printf("������� ����� �������� ��� ������� ����������� �����:\n");
	scanf("%d", &num);
	while(num<1 || num>size)
	{
		printf("������������ ����\n");
		scanf("%d", &num);
	}
	if(a[num-1]>=0)
	{
		printf("���������� ������ �� %d ����� %.2lf\n", a[num-1], sqrt(a[num-1]));
	}
	else
	{
		printf("���������� ������� ���������� ������ �� %d\n", a[num-1]);
	}

	getch();
}
