#include<stdio.h>
#include<conio.h>
#include<locale.h>
#include<math.h>
#include<stdlib.h>
#include<time.h>

main()
{
	setlocale(LC_ALL, "rus");
	const int MAX_SIZE=50;//������������ ������
	int a[MAX_SIZE];//���������� �������
	int i, size;
	printf("������� ���������� ��������� [1...%d]:\n", MAX_SIZE);
	scanf("%d", &size);//���� ������������� ���������� ���������
	
	while(size<1 || size>MAX_SIZE)//�������� ���������
	{
		printf("����������� ����\n");
		scanf("%d", &size);
	}
	
	//���������� ������� ���������� ������� [-50...50]
	srand(time(0));
	for(i=0; i<size; i++)
	{
		a[i]=rand()%101-50;//rand()%(50-(-50+1)+(-50));
	}
	
	//����� �������
	printf("�������� ������\n");
	for(i=0; i<size; i++)
	{
		printf("%d ", a[i]);
	}
	printf("\n");

/*
5. ����������� ��������� ��� ������ � ���������:
�) �������� �������� �� ������� //������� ��������
*/
	int j;
	
	for(i=0; i<size; i++)//���������� ��� �������� �������
	{
		if(a[i]%2==0)//��������� ����� �� �������
		{
			for(j=i; j<size-1; j++)//��������� �������� �� ����� ����������
			{
				a[j]=a[j+1];
			}
			size--;//��������� �� 1 ������������ ������
			i--;//������ ��� �����, ��� �������� �� ������ ���� ������ ������ ���������, ����� ����� ���
		}
	}
	
	printf("������ ����� �������� ������\n");
	for(i=0; i<size; i++)
	{
		printf("%d ", a[i]);
	}
	printf("\n");
	

	getch();
}
