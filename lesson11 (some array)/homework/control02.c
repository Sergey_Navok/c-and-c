#include<stdio.h>
#include<conio.h>
#include<locale.h>
#include<math.h>
#include<stdlib.h>
#include<time.h>

/*
2. ��� ������. ����� ����� ��������� � ������� �������� � �������� - � ��������� ��������.
*/

main()
{
	setlocale(LC_ALL, "rus");
	const int MAX_SIZE=50;//������������ ������
	int a[MAX_SIZE];//���������� �������
	int i, size, sumPos=0, sumNeg=0;//����������
	printf("������� ���������� ��������� [1...%d]:\n", MAX_SIZE);
	scanf("%d", &size);//���� ������������� ���������� ���������
	
	//�������� ���������
	while(size<1 || size>MAX_SIZE)
	{
		printf("����������� ����\n");
		scanf("%d", &size);
	}
	
	//���������� ������� ���������� ������� [0...10]
	srand(time(0));
	for(i=0; i<size; i++)
	{
		a[i]=rand()%10;
	}
	
	//����� ������� ���������������� �������
	printf("�������� ������:\n");
	for(i=0; i<size; i++)
	{
		printf("%2d ", a[i]);
	}
	printf("\n");
	
	
	//�������� �������
	for(i=0; i<size; i++)
	{
		if((i+1)%2==0)
		{
			sumPos=sumPos+a[i];
		}
		else
		{
			sumNeg=sumNeg+a[i];
		}
	}
		
	//����� �����������
	printf("����� ������ ��������� = %d\n", sumPos);
	printf("����� �������� ��������� = %d\n", sumNeg);	
		
	getch();
}
