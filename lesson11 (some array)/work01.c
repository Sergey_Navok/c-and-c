#include<stdio.h>
#include<conio.h>
#include<locale.h>
#include<math.h>
#include<stdlib.h>
#include<time.h>

/*
1.  ����� ����������� ��� ��������. �������� ����� ���������� �������, ��������� � ������ �������� �� ������ �� n ����, 
������� �������� � ���� ��������. �������� ����� ��������� ��������� ������ ������� �� n ����. 
*/

main()
{
	setlocale(LC_ALL, "rus");
	const int MAX_SIZE=100;
	int a1[MAX_SIZE], a2[MAX_SIZE];
	int i, sum1=0, sum2=0, sum, size;
	
	printf("������� ���������� ���� ������ ���������:\n");
	scanf("%d", &size);
	while(size<1 || size>MAX_SIZE)
	{
		printf("������������ ����\n");
		scanf("%d", &size);
	}
	
	srand(time(0));
	for(i=0; i<size; i++)
	{
		a1[i]=rand()%1001;
		a2[i]=rand()%1001;
	}
	
	printf("��������� �������� ���������\n");
	for(i=0; i<size; i++)
	{
		printf("|%2d|%4d|%4d|\n", i+1, a1[i], a2[i]);
	}
	
	for(i=0; i<size; i++)
	{
		sum1=sum1+a1[i];//sum=sum+a1[i]+a2[i];
		sum2=sum2+a2[i];
	}
	
	sum=sum1+sum2;
	
	printf("��������� ������ ������� ��������: %d\n", sum1);
	printf("��������� ������ ������� ��������: %d\n", sum2);
	printf("��������� ������ ���� ���������: %d\n", sum);
	
	getch();
}
