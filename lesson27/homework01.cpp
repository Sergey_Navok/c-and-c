#include<iostream>
#include<Windows.h>
#include<string>

#include<stdlib.h>//srand	rand	
#include<time.h>//time(0)	- ���������� �����, ������ ���������� ������ ��������� � 1 ������ 1970

using namespace std;

class Human{
private:
	string name;
	int age;
	int exp;
	
public:
	Human(string name, int age, int exp) {
		this->name = name;
		this->age = age;
		this->exp = exp;
	}
	
	Human() {
		this->name = "";
		this->age = 0;
		this->exp = 0;
	}
	
	void setName(string name) {
		this->name = name;
		//this->name.shrink_to_fit();
	}
	
	string getName() {
		return this->name;
	}
	
	void setAge(int age) {
		this->age = age;
	}
	
	int getAge() {
		return this->age;
	}
	
	void setExp() {
		this->exp = exp;
	}
	
	int getExp() {
		return this->age;
	}
	
	void info() {
		
	}
	
	virtual bool fire() {
		
	}
	
	virtual ~Human(){
		
	}
};

class Rockie :public Human {
public:
	Rockie(string name, int age, int exp) :Human(name, age, exp) {
		
	}
	
	void info() {
		cout << "����: �������, ���: " << this->getName() << ", ����: " << this->getExp() << ", �������: " << this->getAge() << endl;
	}
	
	bool fire() {
		this->info();
		cout << "��������� ��������: ";
		int x;
		srand(time(0));
		x=rand()%10;
		
		if((x+(0,01 * this->getExp()))>5) {
			return true;
		} else {
			return false;
		}
	}
	
	~Rockie() {
		
	}
};

class Expert :public Human {
public:
	Expert(string name, int age, int exp) :Human(name, age, exp) {
		
	}
	
	void info() {
		cout << "����: �������, ���: " << this->getName() << ", ����: " << this->getExp() << ", �������: " << this->getAge() << endl;
	}
	
	bool fire() {
		this->info();
		cout << "��������� ��������: ";
		int x;
		srand(time(0));
		x=rand()%10;
		
		if((x+(0,05 * this->getExp()))>5) {
			return true;
		} else {
			return false;
		}
	}
	
	~Expert() {
		
	}
	
};

class Veteran :public Human {
public:
	Veteran(string name, int age, int exp) :Human(name, age, exp) {
		
	}
	
	void info() {
		cout << "����: �������, ���: " << this->getName() << ", ����: " << this->getExp() << ", �������: " << this->getAge() << endl;
	}
	
	bool fire() {
		this->info();
		cout << "��������� ��������: ";
		int x;
		srand(time(0));
		x=rand()%10;
		
		if((x + 0,9 - (0,01 * this->getAge()))>5) {
			return true;
		} else {
			return false;
		}
	}
	
	~Veteran() {
		
	}
	
};

int main() {
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	
	Human** humans = NULL;
	int size = 6;

	humans = new Human * [size];
	humans[0] = new Rockie("����", 5, 35);
	humans[1] = new Rockie("����", 10, 27);
	humans[2] = new Expert("����", 15, 35);
	humans[3] = new Expert("����", 5, 25);
	humans[4] = new Veteran("������", 35, 65);
	humans[5] = new Veteran("��������", 45, 55);
	
	for (int i = 0; i < size; i++) {
		cout << i + 1 << ")";
		cout << humans[i]->fire() << endl;
	}

	for (int i = 0; i < size; i++) {
		delete humans[i];
	}

	delete[] humans;
	humans = NULL;
	
	system("pause");
	return 0;
}

