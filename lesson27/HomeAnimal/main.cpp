#include<iostream>
#include<Windows.h>
#include<string>

using namespace std;

class Pets {
private:
	string name;
public:
	Pets(string name) {
		this->name = name;
	}

	Pets() {

	}

	void setName(string name) {
		this->name = name;
		this->name.shrink_to_fit();
	}

	string getName() {
		return this->name;
	}

	virtual void voise() = 0; //������ ������������ �����, �������� ������� ������������ ������. �����, ������� �� �������� ����������.

	virtual void info() = 0;

	virtual ~Pets() {

	}
};

class Dogs :public Pets {
public:
	Dogs(string name) :Pets(name) {

	}

	Dogs() :Pets() {

	}

	void voise() {
		cout << "���-���" << endl;
	}

	void info() {
		cout << "����� �� ������ " << this->getName() << endl;
	}

	virtual ~Dogs() {

	}
};

class Cats :public Pets {
public:
	Cats(string name) :Pets(name) {

	}

	Cats() :Pets() {

	}

	void voise() {
		cout << "���-���" << endl;
	}

	void info() {
		cout << "����� �� ������ " << this->getName() << endl;
	}

	virtual ~Cats(){

	}
};

int main() {
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);

	//Pets* pets = new Pets(); //�������� ������� ����������� ������� ����������
	Pets** pets = NULL;
	int size = 5;

	pets = new Pets * [size]; //������������ ��������� ������ ��� �������, ���������� �� size ���������� ��� ������� ���� Pets
	pets[0] = new Dogs("����");
	pets[1] = new Cats("�����");
	pets[2] = new Cats("�����");
	pets[3] = new Dogs("�����");
	pets[4] = new Cats("������");

	for (int i = 0; i < size; i++) {
		cout << i + 1 << ")";
		pets[i]->info();
		pets[i]->voise();
	}

	for (int i = 0; i < size; i++) {
		delete pets[i];
	}

	delete[] pets;
	pets = NULL;

	system("pause");
	return 0;
}