#include<iostream>
#include<Windows.h>
#include<string>

using namespace std;

class A {
public:
	virtual void info() {
		cout << "� ������ ������ �" << endl;
	}

	virtual ~A() { //���� � ������ ���� ���� ���������� �����, �� � ��� ���� ������������� ������������ ����������� �����
		cout << "�������� ���������� ������ �" << endl;
	}
};

class B :public A {
public:
	void info() {
		cout << "� ������ ������ �" << endl;
	}

	~B() {
		cout << "�������� ���������� ������ �" << endl;
	}
};

class C :public B {
public:
	void info() {
		cout << "� ������ ������ �" << endl;
	}

	~C() {
		cout << "�������� ���������� ������ C" << endl;
	}
};

int main() {
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);

	A* a1 = new A(); //������������ �������� ������� ������ � � ������ ������ ����� ������� � �������� ��������� �1 (������ �)
	B* b1 = new B();
	C* c1 = new C();

	a1->info();
	b1->info();
	c1->info();
	cout << "-----" << endl;

	A* base; //������� ��������� �������� ������
	base = a1;
	base->info();
	delete base;
	cout << "-----" << endl;

	base = b1;
	base->info();
	delete base;
	cout << "-----" << endl;

	base = c1;
	base->info();
	delete base;
	cout << "-----" << endl;



	/*cout << "-----" << endl;
	delete a1;
	a1 = NULL;
	cout << "-----" << endl;
	delete b1;
	b1 = NULL;
	cout << "-----" << endl;
	delete c1;
	c1 = NULL;
	cout << "-----------------" << endl;*/

	system("pause");
	return 0;
}