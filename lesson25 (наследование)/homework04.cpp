#include<iostream>
#include<Windows.h>
#include<string>

using namespace std;

class Detail {
private:
	string name;
public:
	Detail(string name) {
		this->name = name;
		cout << "�������� ����������� � ����������� Detail" << endl;
	}

	Detail() {
		this->name = "";
		cout << "�������� ����������� ��� ���������� Detail" << endl;
	}

	void setName(string name) {
		this->name = name;
	}

	string getName() {
		return this->name;
	}

	void infoDetail() {
		cout << "��������: " << this->name << endl;
	}

	~Detail() {
		cout << "�������� ���������� ��� ���������� Detail" << endl;
	}
};

class Unit : public Detail {
private:
	double wieght;
public:
	Unit(string name, double wieght) :Detail(name) {
		this->wieght = wieght;
		cout << "�������� ����������� � ����������� Unit" << endl;
	}

	Unit() :Detail() {
		this->wieght = 0;
		cout << "�������� ����������� ��� ���������� Unit" << endl;
	}

	void setWieght(double wieght) {
		this->wieght = wieght;
	}

	double getWieght() {
		return this->wieght;
	}
	
	void infoUnit() {
		cout << "��������: " << this->getName() << ", ���: " << this->wieght << endl;
	}

	~Unit() {
		cout << "�������� ���������� ��� ���������� Unit" << endl;
	}
};

class Mechanism : public Unit {
private:
	char leftRight;
public:
	Mechanism(string name, double wieght, char leftRight) :Unit(name, wieght) {
		this->leftRight = leftRight;
		cout << "�������� ����������� � ����������� Mechanism" << endl;
	}

	Mechanism() :Unit() {
		this->leftRight = 0;
		cout << "�������� ����������� ��� ���������� Mechanism" << endl;
	}

	void setLeftRight(char leftRight) {
		this->leftRight = leftRight;
	}

	char getLeftRight() {
		return this->leftRight;
	}
	
	void infoMechanism() {
		cout << "��������: " << this->getName() << ", ���: " << this->getWieght() << ", �����/������: "<< this->leftRight << endl;
	}

	~Mechanism() {
		cout << "�������� ���������� ��� ���������� Mechanism" << endl;
	}
};

class Product : public Mechanism {
private:
	double price;
public:
	Product(string name, double wieght, char leftRight, double price) :Mechanism(name, wieght, leftRight) {
		this->price = price;
		cout << "�������� ����������� � ����������� Product" << endl;
	}

	Product() :Mechanism() {
		this->price = 0;
		cout << "�������� ����������� ��� ���������� Product" << endl;
	}

	void setPrice(double price) {
		this->price = price;
	}

	double getPrice() {
		return this->price;
	}
	
	void infoProduct() {
		cout << "��������: " << this->getName() << ", ���: " << this->getWieght() << ", �����/������: " << this->getLeftRight() << ", ����: "<< this->price << endl;
	}

	~Product() {
		cout << "�������� ���������� ��� ���������� Product" << endl;
	}
};

int main() {
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);

	Detail* detail = new Detail ("����");
	detail->infoDetail();
	delete detail;
	detail = NULL;

	cout << "--------------------------------" << endl;

	Unit* unit = new Unit("�����", 120.5);
	unit->infoUnit();
	delete unit;
	unit = NULL;
	
	cout << "--------------------------------" << endl;

	Mechanism* mechanism = new Mechanism("�����", 60.5, '�');
	mechanism->infoMechanism();
	delete mechanism;
	mechanism = NULL;
	
	cout << "--------------------------------" << endl;

	Product* product = new Product("�����", 15000, '�', 150);
	product->infoProduct();
	delete product;
	product = NULL;

	system("pause");
	return 0;
}
