#include<iostream>
#include<Windows.h>
#include<string>

using namespace std;

class Human {
private:
	string name;
	int age;
public:
	Human(string name, int age) {
		this->name = name;
		this->age = age;
		cout << "�������� ����������� � ����������� Human" << endl;
	}

	Human() {
		this->name = "";
		this->age = 0;
		cout << "�������� ����������� ��� ��������� Human" << endl;
	}

	void setName(string name) {
		this->name = name;
		this->name.shrink_to_fit();
	}

	string getName() {
		return this->name;
	}

	void setAge(int age) {
		this->age = age;
	}

	int getAge() {
		return this->age;
	}

	void infoHuman() {
		cout << "���: " << this->name << ", �������: " << this->age << endl;
	}
	
	~Human() {
		cout << "�������� ���������� Human" << endl;
	}
};

class Worker : public Human {
private:
	double salary;
public:
	Worker(string name, int age, double salary) :Human(name, age) {
		this->salary = salary;
		cout << "�������� ����������� � ����������� Worker" << endl;
	}

	Worker() :Human() {
		this->salary = 0;
		cout << "�������� ����������� ��� ���������� Worker" << endl;
	}

	void setSalary(double salary) {
		this->salary = salary;
	}

	double getSalary() {
		return this->salary;
	}

	void infoWorker() {
		cout << "���: " << this->getName() << ", �������:" << this->getAge() << ", ��������: " << this->salary << endl;
	}

	~Worker() {
		cout << "�������� ���������� Worker" << endl;
	}
};

int main() {
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);

	Human* human = new Human("����", 25);
	human->infoHuman();
	delete human;
	human = NULL;

	cout << "--------------------------------" << endl;

	Worker* worker = new Worker("��������", 34, 500);
	worker->infoWorker();
	delete worker;
	worker = NULL;

	system("pause");
	return 0;
}