#include<iostream>
#include<Windows.h>
#include<string>

using namespace std;

class Human {
private:
	string name;
public:
	Human(string name) {
		this->name = name;
	}

	Human() {
		this->name = "";
	}

	void setName(string name) {
		this->name = name;
	}

	string getName() {
		return this->name;
	}

	void info() {
		cout << "���: " << this->name << endl;
	}

	~Human() {

	}
};

class Worker :virtual public Human {
private:
	double salary;
public:
	Worker(string name, double salary) :Human(name) {
		this->salary = salary;
	}

	Worker(double salary) {
		this->salary = salary;
	}

	Worker() :Human() {
		this->salary = 0;
	}

	void setSalary(double salary) {
		this->salary = salary;
	}

	double getSalary() {
		return this->salary;
	}

	void info() {
		cout << "���: " << this->getName() << ", ��������: " << this->salary << endl;
	}

	~Worker() {

	}
};

class Student :virtual public Human {
private:
	int course;
public:
	Student(string name, int course) :Human(name) {
		this->course = course;
	}

	Student(int course) {
		this->course = course;
	}

	Student() :Human() {
		this->course = 1;
	}

	void setCourse() {
		this->course = course;
	}

	int getCourse() {
		return this->course;
	}

	void info() {
		cout << "���: " << this->getName() << ", ����: " << this->course << endl;
	}

	~Student() {

	}
};

class Superman :public Worker, public Student {
private:
	bool isFly;
public:
	Superman(string name, double salary, int course, bool isFly) :Human(name), Worker(salary), Student(course) {
		this->isFly = isFly;
	}

	Superman(): Worker(), Student() {
		this->isFly = false;
	}

	void setIsFly(bool isFly) {
		this->isFly = isFly;
	}

	bool getIsFly() {
		return this->isFly;
	}

	void info() {
		cout << "���: " << this->getName() << ", ��������: " << this->getSalary() << ", ����: " << this->getCourse() << ", ������: " << this->isFly << endl;
	}

	~Superman() {

	}
};

int main() {
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);

	Superman* sup = new Superman("�����", 500, 3, false);
	sup->info();

	delete sup;
	sup = NULL;

	system("pause");
	return 0;
}