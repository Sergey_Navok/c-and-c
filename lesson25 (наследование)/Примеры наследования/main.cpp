#include<iostream>
#include<Windows.h>
#include<string>

using namespace std;

class Human {
private:
	string name;
public:
	Human(string name) {
		this->name = name;
		cout << "�������� ����������� � ����������� Human" << endl;
	}

	Human() {
		this->name = "";
		cout << "�������� ����������� ��� ���������� Human" << endl;
	}

	void setName(string name) {
		this->name = name;
	}

	string getName() {
		return this->name;
	}

	void infoHuman() {
		cout << "���: " << this->name << endl;
	}

	~Human() {
		cout << "�������� ���������� ��� ���������� Human" << endl;
	}
};

class Student : public Human {
private:
	int course;
public:
	Student(string name, int course) :Human(name) {
		this->course = course;
		cout << "�������� ����������� � ����������� Student" << endl;
	}

	Student() :Human() {
		this->course = 0;
		cout << "�������� ����������� ��� ���������� Human" << endl;
	}

	void setCourse(int course) {
		this->course = course;
	}

	int getCourse() {
		return this->course;
	}
	
	void infoStudent() {
		cout << "���: " << this->getName() << ", ����: " << this->course << endl;
	}

	~Student() {
		cout << "�������� ���������� ��� ���������� Student" << endl;
	}
};

class Mentor : public Human {
private:
	double salary;
public:
	Mentor(string name, double salary) :Human(name) {
		this->salary = salary;
		cout << "�������� ����������� � ����������� Mentor" << endl;
	}

	Mentor() :Human() {
		this->salary = 0;
		cout << "�������� ����������� ��� ���������� Mentor" << endl;
	}

	void setSalary(double salary) {
		this->salary = salary;
	}

	double getSalary() {
		return this->salary;
	}

	void infoMentor() {
		cout << "���: " << this->getName() << ", ��������: " << this->salary << endl;
	}

	~Mentor() {
		cout << "�������� ���������� ��� ���������� Mentor" << endl;
	}
};

class Boss : public Mentor {
private:
	int stupen;
public:
	Boss(string name, double salary, int stupen) :Mentor(name, salary) {
		this->stupen = stupen;
		cout << "�������� ����������� � ����������� Boss" << endl;
	}

	Boss() :Mentor() {
		this->stupen = 0;
		cout << "�������� ����������� ��� ���������� Boss" << endl;
	}

	void setStupen(int stupen) {
		this->stupen = stupen;
	}

	int getStupen() {
		return this->stupen;
	}

	void infoBoss() {
		cout << "���: " << this->getName() << ", ��������: " << this->getSalary()<< ", �������: "<< this->stupen << endl;
	}

	~Boss() {
		cout << "�������� ���������� ��� ���������� Boss" << endl;
	}
};

int main() {
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);

	Human* human = new Human("����");
	human->infoHuman();
	delete human;
	human = NULL;

	cout << "--------------------------------" << endl;

	Student* student = new Student("�������", 4);
	student->infoStudent();
	delete student;
	student = NULL;

	cout << "--------------------------------" << endl;

	Mentor* mentor = new Mentor("����", 500.25);
	mentor->infoMentor();
	delete mentor;
	mentor = NULL;

	cout << "--------------------------------" << endl;

	Boss* boss = new Boss("��������", 100.5, 5);
	boss->infoBoss();
	delete boss;
	boss = NULL;

	system("pause");
	return 0;
}
