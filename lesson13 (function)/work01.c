#include<stdio.h>
#include<conio.h>
#include<locale.h>
#include<math.h>
#include<stdlib.h>
#include<time.h>

/*
1. ������� ������� CircleS(R) ������������� ����, ��������� ������� ����� ������� R (R - ������������). 
� ������� ���� ������� ����� ������� ���� ������ � ������� ���������. 
������� ����� ������� R ����������� �� ������� S = ��R2.
*/

double circlesS(double);

int main()
{
	setlocale(LC_ALL, "rus");
	double r1, r2, r3, s1, s2, s3;
	printf("������� ������� ���� ������:\n");
	scanf("%lf%lf%lf", &r1, &r2, &r3);
	s1=circlesS(r1);
	s2=circlesS(r2);
	s3=circlesS(r3);
	
	printf("�������: 1) %.2lf, 2) %.2lf, 3) %.2lf", s1, s2, s3);
	
	
	getch();
	return 0;
}

double circlesS (double R)
{
	double res;
	res=M_PI*pow(R, 2);
	return res;
}
