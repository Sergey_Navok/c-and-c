#include<stdio.h>
#include<conio.h>
#include<locale.h>
#include<math.h>
#include<stdlib.h>
#include<time.h>

/*
6. ������� ������� Fact(N) ������ ����, ����������� �������� ���������� N! = 1�2���N 
(N > 0 - �������� ������ ���� (��������, �������� ����������� ������������ ������� ��������). 
� ������� ���� ������� ����� ���������� ���� ������ ����� �����.
*/

void fact(int);

int main()
{
	setlocale(LC_ALL, "rus");
	int n;
	printf("������� N! ��� ������� ����������:\n");
	scanf("%d", &n);
	
	while(n<1)
	{
		printf("������������ ���� �����!\n");
		scanf("%d", &n);
	}
	
	fact(n);
	
	//����� �������		
	getch();
	return 0;
}

//�������� (����������) �������
void fact (int number)
{
	int result=1, b=0, i;
	for(i=1; i<=number; i++)
	{
		result=result*i;
		if(result>2000000000)
		{
			result=result/i*100;
			b++;
		}
	}
	
	if(b>0)
		{
			printf("�������� ���������� ����� �� ������� ������\n");
		}
		else
		{
			printf("�������� ���������� = %d\n", result);
		}
}
