#include<stdio.h>
#include<conio.h>
#include<locale.h>
#include<math.h>
#include<stdlib.h>
#include<time.h>

/*
7. �������� ��� ������������ ���������� ������. ����������  �������� ����� ������������ �����, 
� ������� ����� ��� ������ ���� ���� ����� ����� ��� ��������� ���� ����. 
(���������� ������� ��� ������� ����� ���� ������������ �����.)
*/

int lucky(int);

int main()
{
	setlocale(LC_ALL, "rus");
	int num, i;
	printf("������� ������������ �����:\n");
	scanf("%d", &i);
	
	while(i<100000 || i>1000000)
	{
		printf("������������ ���� �����!\n");
		scanf("%d", &i);
	}
	
	num=lucky(i);
	
	if(num!=0)
		{
			printf("������� ���������� �����: %d\n", num);
		}
	else
	{
		printf("������� ������������ �����!\n");
	}
	
	//����� �������		
	getch();
	return 0;
}

//�������� (����������) �������
int lucky (int number)
{
	int i, b1, b2, c, res=0;
	int a[6];
	c=number;
	
	for(i=1; i<=6; i++)
	{
		a[i]=c%10;
		c=c/10;
	}
	
	b1=a[1]+a[2]+a[3];
	b2=a[4]+a[5]+a[6];
	
	if(b1==b2)
	{
		res=number;
	}	
	return res;
}
