#include<stdio.h>
#include<conio.h>
#include<locale.h>
#include<math.h>
#include<stdlib.h>
#include<time.h>

/*
3. ������� ������� Calc(A, B, Op) ������������� ����, ����������� ��� ���������� ������������� 
������� A � B ���� �� �������������� �������� � ������������ �� ���������. ��� �������� ������������ 
����� ���������� Op: 1 - ��������, 2 - ���������, 3 - ���������, 4 - �������. � ������� Calc ��������� ��� 
������ A � B ���������� ��� ��������.
*/

double calc(double, double, int);

int main()
{
	setlocale(LC_ALL, "rus");
	double a, b, result;
	int operation;
	printf("������� ��� ����� ����� � �.�. � �������, �� ����� 0\n");
	
	//���� � �������� ����� ����� �
	printf("������� ������ �����:\n");
	scanf("%lf", &a);	
	while(a==0)
	{
		printf("������������ ���� �����!\n");
		scanf("%lf", &a);
	}
	
	//���� � �������� ����� ����� �
	printf("������� ������ �����:\n");
	scanf("%lf", &b);
	while(b==0)
	{
		printf("������������ ���� �����!\n");
		scanf("%lf", &b);
	}

	//����� ��������
	do
	{
		printf("�������� �������������� �������� (���� 0 ��������� ������):\n");
		printf("1. ��������\n");
		printf("2. ���������\n");
		printf("3. ���������\n");
		printf("4. �������\n");
		scanf("%d", &operation);
		
		while(operation<0 || operation>4)//�������� ������� ��������
		{
			printf("������������ ����� ��������!\n");
			scanf("%d", &operation);
		}
		
		if(operation!=0)
		{
			result=calc(a, b, operation);
			printf("��������� %d �������� = %.2lf\n\n\n", operation, result);
		}
		else
		{
			printf("������ ��������� ����� ���������. ������� ����� �������.\n");
		}
		
	}while(operation!=0);

	//����� �������	
	getch();
	return 0;
}

//�������� (����������) �������
double calc (double a1, double b1, int op)
{
	double res;
	switch(op)
	{
		case 1://�������� ��������
			res=a1+b1;
		break;
		
		case 2://�������� ���������
			res=a1-b1;
		break;
		
		case 3://�������� ���������
			res=a1*b1;
		break;
		
		case 4://�������� �������
			res=a1/b1;
		break;
	}
	return res;
}
