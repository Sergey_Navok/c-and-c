#include<stdio.h>
#include<conio.h>
#include<locale.h>
#include<math.h>
#include<stdlib.h>
#include<time.h>

/*
4. ������� ������� Quarter(x, y) ������ ����, ������������ ����� ������������ ��������, 
� ������� ��������� ����� � ���������� ������������� ������������ (x, y). 
� ������� ���� ������� ����� ������ ������������ ��������� ��� ���� ����� � ������� ���������� ������������. 
*/

void quarter (double, double);

int main()
{
	setlocale(LC_ALL, "rus");
	double x, y;
	int i;
	
	for(i=1; i<4; i++)
	{
		//�������� ���������� �� �
		printf("������� %d-� ���������� ����� �� X (�������� ����� 0):\n", i);
		scanf("%lf", &x);	
		while(x==0)
		{
			printf("������������ ���� ���������� X!\n");
			scanf("%lf", &x);
		}
		
		//�������� ���������� �� �
		printf("������� %d-� ���������� ����� �� Y (�������� ����� 0):\n", i);
		scanf("%lf", &y);	
		while(y==0)
		{
			printf("������������ ���� ���������� Y!\n");
			scanf("%lf", &y);
		}
		
		quarter(x, y);		
	}
	
	//����� �������
	getch();
	return 0;
}

//�������� (����������) �������
void quarter(double x1, double y1)
{
	int res;
	if((x1>0 && y1>0) || (x1<0 && y1>0) || (x1>0 && y1<0))
	{
		if((x1>0 && y1>0) || (x1<0 && y1>0))
		{
			if(x1>0 && y1>0)
			{
				res=1;
			}
			else
			{
				res=2;
			}
		}
		else
		{
			res=4;
		}
	}
	else
	{
		res=3;
	}
	
	printf("����� ��������� � %d-� ������������ ��������\n\n\n", res);
}
