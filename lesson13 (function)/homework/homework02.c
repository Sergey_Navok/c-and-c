#include<stdio.h>
#include<conio.h>
#include<locale.h>
#include<math.h>
#include<stdlib.h>
#include<time.h>

/*
2. ������� ������� SumRange(A, B) ������ ����, ��������� ����� ���� ����� ����� 
�� A �� B ������������ (A � B - �����). ���� A >B, �� ������� ���������� 0. � ������� ���� ������� 
����� ����� ����� �� A �� B � �� B �� C, ���� ���� ����� A, B, C.
*/

int sumRange(int, int);

int main()
{
	setlocale(LC_ALL, "rus");
	int a, b, c, summa, switch_value;
	printf("��������, � ����� ��� ���� ���������� ��������� ����� �����:\n");
	scanf("%d", &switch_value);//���� ���������� ����������
	
	//�������� ������ ���������
	while(switch_value<1 || switch_value>2)
	{
		printf("������������ ����� ���������!\n");
		scanf("%d", &switch_value);
	}
	
	//����� �������� �������
	switch(switch_value)
	{
		case 1://����� � ��������� �� A �� B
			printf("������ ����� ����� � ��������� �� A �� B!\n");
			printf("������� �������� A � B:\n");
			scanf("%d%d", &a, &b);
			
			summa=sumRange(a, b);//����� �������
			
			//����� ����������� � ����������� �� ������� �����
			if(summa!=0)
			{
				printf("����� ���� ����� � ��������� �� � �� � = %d", summa);
			}
			else
			{
				printf("�������� � ������ �������� �!\n");
			}			
		break;
		
		case 2://����� � ��������� �� A �� B � �� B �� C
			printf("������ ����� ����� � ��������� �� A �� B � �� � �� �!\n");
			printf("������� �������� A, B � �:\n");
			scanf("%d%d%d", &a, &b, &c);
			
			//����� ������� ��� ������� ���������
			summa=sumRange(a, b);
			
			//����� ����������� � ����������� �� ������� �����
			if(summa!=0)
			{
				printf("����� ���� ����� � ��������� �� � �� � = %d\n", summa);
			}
			else
			{
				printf("�������� � ������ �������� �!\n");
			}
			
			//����� ������� ��� ������� ���������
			summa=sumRange(b, c);
			
			//����� ����������� � ����������� �� ������� �����
			if(summa!=0)
			{
				printf("����� ���� ����� � ��������� �� � �� � = %d\n", summa);
			}
			else
			{
				printf("�������� � ������ �������� �!\n");
			}
		break;
	}	
	//����� �������		
	getch();
	return 0;
}

//�������� (����������) �������
int sumRange (int num1, int num2)
{
	int i, res=0;	
	if(num1<num2)
	{
		for(i=num1; i<=num2; i++)
		{
			res=res+i;
		}
	}	
	return res;
}
