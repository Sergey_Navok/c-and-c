#include<stdio.h>
#include<conio.h>
#include<locale.h>
#include<math.h>
#include<stdlib.h>
#include<time.h>

/*
5. ������� ������� DigitCount(K) ������ ����, ��������� ���������� ���� ������ �������������� ����� K. 
��������� ��� �������, ����� ���������� ���� ��� ������� �� ���� ������ ����� ������������� �����.
*/

int digitCount(int);

int main()
{
	setlocale(LC_ALL, "rus");
	int k, i, num;
	
	//������� ��� ����� 5 �����
	for(i=1; i<6; i++)
	{
		printf("������� %d-� ������������� ����� �� 5:\n", i);
		scanf("%d", &k);
		
		//�������� ����� �������������� �����
		while(k<1)
		{
			printf("������������ ���� �����!\n");
			scanf("%d", &k);
		}
		
		//������ �������
		num=digitCount(k);
		
		//����� �����������
		printf("����� %d �������� � ���� %d �����(�)\n", k, num);
	}

	//����� �������		
	getch();
	return 0;
}

//�������� (����������) �������
int digitCount (int number)
{
	int i, res=0;	
	for(i=number; i>0; i=i/10)
	{
		res++;
	}
	return res;
}
