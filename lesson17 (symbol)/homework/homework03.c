#include<stdio.h>
#include<conio.h>
#include<math.h>
#include<stdlib.h>
#include<time.h>
#include<windows.h>

/*
3. ��� ������ C, ������������ ����� ��� ����� (��������� ��� ���-����). 
���� C ���������� �����, �� ������� ������ "digit", ���� ��������� ����� - ������� ������ "lat", 
���� ������� - ������� ������ "rus".
*/

main()
{
	SetConsoleCP(1251);//��������� win 1251 �� ����������� ������� ����� stdin
	SetConsoleOutputCP(1251);//��������� win 1251 �� ����������� �������� ����� stdout
	char symbol;
	printf("������� ������:\n");
	scanf("%c", &symbol);
	if(symbol>='0' && symbol<='9')
	{
		printf("������� �����\n");
	}
	else
	{
		if((symbol>='A' && symbol<='Z') || (symbol>='a' && symbol<='z'))
		{
			printf("������� ��������� �����\n");
		}
		else
		{
			if((symbol>='�' && symbol<='�') || (symbol>='�' && symbol<='�') || (symbol=='�' && symbol=='�'))
			{
				printf("������� ������� �����\n");
			}
			else
			{
				printf("��������� ������ �� �������� ������ ��� ������\n");
			}
		}
	}
	
	printf("�� ������� ��������� <<%c>> ����� ��� - %d\n", symbol, (unsigned char)symbol);
	getch();
	return 0;
}
