#include<iostream>
#include<Windows.h>
#include<string>

using namespace std;

class Car {
private:
	string mark;
	int maxSpeed;
public:
	Car(string mark, int maxSpeed) {
		this->mark = mark;
		this->maxSpeed = maxSpeed;
	}

	Car() {
		this->mark = "";
		this->maxSpeed = 0;
	}

	void setMark(string mark) {
		this->mark = mark;
		this->mark.shrink_to_fit();
	}

	string getMark() {
		return this->mark;
	}

	void setMaxSpeed(int maxSpeed) {
		this->maxSpeed = maxSpeed;
	}

	int getMaxSpeed() {
		return this->maxSpeed;
	}

	int cost() {
		return (this->maxSpeed * 100);
	}

	void update() {
		this->maxSpeed += 10;
		cout << "���������� ������ ������� ���������!" << endl;
	}

	void update(int addSpeed) {
		this->maxSpeed = this->maxSpeed + addSpeed;
		cout << "�������� ��������� �� " << addSpeed << " ��/�" << endl;
	}

	void info() {
		cout << "�����: " << this->mark << ", ���� ��������: " << this->maxSpeed << "��/�, ���������: " << this->cost() << endl;
	}

	~Car() {

	}
};

class VipCar: public Car {
public:
	VipCar(string mark, int maxSpeed) : Car(mark, maxSpeed) {

	}

	VipCar(): Car() {

	}

	int cost() {
		return (this->getMaxSpeed() * 250);
	}

	void update() {
		this->setMaxSpeed(this->getMaxSpeed() + 5);
	}

	void info() {
		cout << "�����: " << this->getMark() << ", ���� ��������: " << this->getMaxSpeed() << "��/�, ���������: " << this->cost() << endl;
	}

	~VipCar() {

	}
};

int main() {
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);

	Car* car1 = new Car("Opel", 200);
	car1->info();
	car1->update();
	car1->info();
	car1->update(9);
	car1->info();
	delete car1;
	car1 = NULL;

	VipCar* vc1 = new VipCar("Ferrari", 320);
	vc1->info();
	vc1->update();
	vc1->info();
	delete vc1;
	vc1 = NULL;

	system("pause");
	return 0;
}