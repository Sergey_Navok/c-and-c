#include<iostream>
#include<Windows.h>
#include<string>

using namespace std;

class Student {
private:
	string firstName;
	string lastName;
	string faculty;
	int course;
	int minGrade;
public:
	Student(string firstName, string lastName, string faculty, int course, int minGrade) {
		this->firstName = firstName;
		this->lastName = lastName;
		this->faculty = faculty;
		this->course = course;
		this->minGrade = minGrade;
	}
	
	Student() {
		this->firstName = "����";
		this->lastName = "������";
		this->faculty = "�����";
		this->course = 1;
		this->minGrade = 0;
	}
	
	void setFirstName(string firstName) {
		this->firstName = firstName;
		//this->firstName.shrink_to_fit();
	}
	
	string getFirstName() {
		return this->firstName;
	}
	
	void setLastName(string lastName) {
		this->lastName = lastName;
		//this->lastName.shrink_to_fit();
	}
	
	string getLastName() {
		return this->lastName;
	}
	
	void setFaculty(string faculty) {
		this->faculty = faculty;
		//this->faculty.shrink_to_fit();
	}
	
	string getFaculty(){
		return this->faculty;
	}
	
	void setCourse(int course) {
		this->course = course;
	}
	
	int getCourse() {
		return this->course;
	}
	
	void setMinGrade(int minGrade) {
		this->minGrade = minGrade;
	}
	
	int getMinGrade() {
		return this->minGrade;
	}
	
	//������
	void nextCourse() {
		if(this->minGrade >= 3) {
			this->course += 1;
			cout << this->lastName << " ��������� �� ����. ����." << endl;
		} else {
			cout << this->lastName << " ����� ������." << endl;
		}
	}
	
	int grants() {
		if(this->minGrade >= 3) {
			return (this->minGrade * 10);
			//cout << "���������: " << (this->minGrade * 10) << "���." << endl;
		} else {
			return 0;
			//cout << "��������� �� ���������." << endl;
		}
	}
	
	void info() {
		cout << "���: " << this->firstName << " " << this->lastName << ", ���������: " << this->faculty
			 << ", ����: " << this->course << ", ���.������: " << this->minGrade << ", ���������: "
			 << this->grants() << "���." << endl;
	}
	
	~Student() {
		//cout << "�������� ����������" << endl;
	}
};

class Contract: public Student {
private:
	bool paid;
public:
	Contract (string firstName, string lastName, string faculty, int course, int minGrade, bool paid) : Student(firstName, lastName, faculty, course, minGrade) {
		this->paid = paid;
	}
	
	Contract() : Student() {
		this->paid = false;
	}
	
	void setPaid(bool paid) {
		this->paid = paid;
	}
	
	bool getPaid() {
		return this->paid;
	}
	
	//������
	void nextCourse() {
		if(this->getMinGrade() >= 3 && this->paid == true) {
			this->setCourse(getCourse() + 1);
			cout << this->getLastName() << " ��������� �� ����. ����." << endl;
		} else {
			cout << this->getLastName() << " ����� ������ ��� �� ������." << endl;
		}
	}
	
	int grants() {
		return 0;
	}
	
	void info() {
		cout << "���: " << this->getFirstName() << " " << this->getLastName() << ", ���������: " << this->getFaculty()
			 << ", ����: " << this->getCourse() << ", ���.������: " << this->getMinGrade() << ", ���������: "
			 << this->grants() << "���." << ", �������: " << this->paid << endl;
	}
	
	~Contract() {
		//cout << "�������� ����������" << endl;
	}
};

int main() {
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	
	Student* student1 = new Student("����", "������", "�����", 4, 3);
	Student* student2 = new Student();
	student1->info();
	student2->info();
	student1->nextCourse();
	student2->nextCourse();
	student1->info();
	student2->info();
	
	delete student1;
	student1 = NULL;
	delete student2;
	student2 = NULL;
	
	cout << endl;
	
	Contract* contract1 = new Contract("����", "���������", "���-��", 3, 5, true);
	Contract* contract2 = new Contract();
	contract1->info();
	contract2->info();
	contract1->nextCourse();
	contract2->nextCourse();
	contract1->info();
	contract2->info();
	
	delete contract1;
	contract1 = NULL;
	delete contract2;
	contract2 = NULL;
	
	system("pause");
	return 0;
}
