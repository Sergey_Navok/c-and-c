#include<iostream>
#include<Windows.h>
#include<string>
#include<vector>
#include<algorithm>

#include<typeinfo>//������ ��� �++

using namespace std;

class Human {
private:
	string name;
	int age;
public:
	Human(string name, int age) {
		this->name = name;
		this->age = age;
	}
	
	Human() {
		this->name = "";
		this->age = 0;
	}
	
	void setName(string name) {
		this->name = name;
	}
	
	string getName() {
		return this->name;
	}
	
	void setAge(int age) {
		this->age = age;
	}
	
	int getAge() {
		return this->age;
	}
	
	virtual void hello(Human*) = 0;
	virtual void info() = 0;
	
	virtual ~Human() {
		
	}
};

class Badman :public Human{
public:
	Badman(string name, int age) :Human(name, age) {
		
	}
	
	Badman() :Human() {
		
	}
	
	void hello(Human* human) {
		cout << this->getName() << ": �����, ������� " << human->getName() << "!" << endl;
	}
	
	void info() {
		cout << "���� �����" << this->getName() << ", ���: " << this->getAge() << " ���, � � ������!" << endl;
		//typeid(Badman).name();
	}
	
	void voice() {
		cout << "���� �����....." << endl;
	}
	
	virtual ~Badman() {
		
	}
};

class Happyman :public Human {
public:
	Happyman(string name, int age) :Human(name, age) {
		
	}
	
	Happyman() :Human() {
		
	}
	
	void hello(Human* human) {
		cout << this->getName() << ": ������, " << human->getName() << "!" << endl;
	}
	
	void info() {
		cout << "���� �����" << this->getName() << ", ���: " << this->getAge() << " ���, � � ��������!" << endl;
	}
	
	virtual ~Happyman() {
		
	}
};

class Smartman :public Human {
public:
	Smartman(string name, int age) :Human(name, age) {
		
	}
	
	Smartman() :Human() {
		
	}
	
	void hello(Human* human) {
		if(human->getAge() - this->getAge() > 5) {
			cout << this->getName() << ": ����������� ���, " << human->getName() << "!" << endl;
		} else {
			cout << this->getName() << ": ����������, " << human->getName() << "!" << endl;
		}
	}
	
	void info() {
		cout << "���� �����" << this->getName() << ", ���: " << this->getAge() << " ���, � � �����!" << endl;
	}
	
	virtual ~Smartman() {
		
	}
};

int main() {
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	
	vector <Human*> humans;
	humans.push_back(new Badman("�����", 18));
	humans.push_back(new Happyman("�����", 19));
	humans.push_back(new Smartman("������", 27));
	humans.push_back(new Badman("�����", 35));
	humans.push_back(new Happyman("�����", 33));
	humans.push_back(new Smartman("����������", 20));
	
	for (int i = 0; i<humans.size(); i++) {
		for (size_t j = i+1; j<humans.size(); j++) {//��� �������������� int
			humans[i]->hello(humans[j]);
			humans[j]->hello(humans[i]);
		}
	}
	
	for (size_t i = 0; i<humans.size(); i++) {
		if (typeid(*humans[i]) == typeid(Badman)) { //���������� � �������� �� ������ � ������� �� ��� ���
			//�� ����� typeid(Humans*) == typeid(Badman)
			((Badman*)humans[i])->voice();
			
		}
	}

	system("pause");
	return 0;
}
