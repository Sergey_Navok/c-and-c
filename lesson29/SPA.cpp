#include<iostream>
#include<Windows.h>
#include<string>
#include<vector>
#include<algorithm>

#include<typeinfo>//������ ��� �++

using namespace std;

class Service {
private:
	string name;
	double cost;
public:
	Service (string name, double cost) {
		this->name = name;
		this->cost = cost;
	}
	
	Service() {
		this->name = "";
		this->cost = 0;
	}
	
	void setName(string name) {
		this->name = name;
	}
	
	string getName() {
		return this->name;
	}
	
	void setCost(double cost) {
		this->cost = cost;
	}
	
	double getCost() {
		return this->cost;
	}
	
	virtual void info() = 0;
	
	virtual ~Service() {
		
	}
};
//---------------------------------------------//

class StandartService :public Service {
public:
	StandartService(string name, double cost) :Service(name, cost) {
		
	}
	
	StandartService() :Service() {
		
	}
	
	void info() {
		cout << "������: " << this->getName() << ", ���������: " << this->getCost() << endl;
	}
	
	virtual ~StandartService() {
		
	}
};
//---------------------------------------------//

class VipService :public Service {
public:
	VipService(string name, double cost) :Service(name, cost) {
		
	}
	
	VipService() :Service() {
		
	}
	
	void info() {
		cout << "������ (VIP): " << this->getName() << ", ���������: " << this->getCost() << endl;
	}
	
	virtual ~VipService() {
		
	}
};
//---------------------------------------------//

class Client {
private:
	static double vipLimit;
	string name;
	//---------------------------------------------//
	//---------------------------------------------//
	string service[];
	//---------------------------------------------//
	//---------------------------------------------//
	double balance;
public:
		Client (string name, string service, double balance) {
		this->name = name;
		this->service[0] = service;
		this->balance = balance;
	}
	
	Client (string name, double balance) {
		this->name = name;
		this->service[0] = "";
		this->balance = balance;
	}
	
	Client() {
		this->name = "";
		this->service[0] = "";
		this->balance = 0;
	}
	
	static void setVipLimit(double vipLimit) {
		Client::vipLimit = vipLimit;
	}
	
	static double getVipLimit() {
		return Client::vipLimit;
	}
	
	void setName(string name) {
		this->name = name;
	}
	
	string getName() {
		return this->name;
	}
	
	void setSerivice(string service) {
		//---------------------------------------------//
	}
	
	string getService() {
		//---------------------------------------------//
	}
	
	void setBalance(double balance) {
		this->balance = balance;
	}
	//---------------------------------------------//
	//���������� ����� ������� 2 �������, �� ����� ��������� �� ���� ����. �� ����� ������� ������ � �������������� �����
	//---------------------------------------------//
	void changeBalance(double balance) {
		this->balance += balance;
	}
	//---------------------------------------------//
	//---------------------------------------------//
	
	double getBalance() {
		return this->balance;
	}
	
	virtual void info() = 0;
	
	virtual ~Client() {
		
	}
};

double Client::vipLimit = 500;
//---------------------------------------------//

class StandartClient :public Client {
public:
	StandartClient(string name, string service, double balance) :Client(name, service, balance) {
		
	}
	
	StandartClient(string name, double balance) :Client(name, balance) {
		
	}
	
	StandartClient() :Client() {
		
	}
	
	void setBalance(double balance) {
		this->setBalance(this->getBalance() + balance);
	}
	
	void info() {
		cout << "������: " << this->getName() << " , ������: " << this->getBalance() << ", ������ �����: " << this->getService() << endl;
	}
	
	virtual ~StandartClient() {
		
	}
};
//---------------------------------------------//

class VipClient :public Client {
private:
	static double bonus;
	static double cashback;
public:
	VipClient(string name, string service, double balance) :Client(name, service, balance) {
		
	}
	
	VipClient(string name, double balance) :Client(name, balance) {
		
	}
	
	VipClient() :Client() {
		
	}
	
	static void setBonus(double bonus) {
		VipClient::bonus = bonus;
	}
	
	static double getBonus() {
		return VipClient::bonus;
	}
	
	static void setCashback(double cashback) {
		VipClient::cashback = cashback;
	}
	
	static double getCashback() {
		return VipClient::cashback;
	}
	
	void setBalance(double balance) {
		this->setBalance(this->getBalance() + (balance * (VipClient::bonus / 100 + 1)));
	}
	
	void info() {
		cout << "������ (VIP): " << this->getName() << " , ������: " << this->getBalance() << ", ������ �����: " << this->getService() << endl;
	}
	
	virtual ~VipClient() {
		
	}
};

double VipClient::bonus = 5;
double VipClient::cashback = 3;
//---------------------------------------------//

int main() {
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	
	

	system("pause");
	return 0;
}
