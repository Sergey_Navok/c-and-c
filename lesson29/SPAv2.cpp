#include<iostream>
#include<Windows.h>
#include<string>
#include<vector>
#include<algorithm>

#include<typeinfo>//������ ��� �++

/*
����������� �������������-��������� ������� ��� ������ ��� ������. 
��� ����� ������������� ����� ��������� ����� (������������, ���������). 
������ ������� �� 2 ����: �������� � �������. ����� � ��� ������ ���� 
������ �������� (���, ������� � ��� ������, ������ ��������� �����), 
������� ������� �� ������� � ������� ��������. ��� ������� �������� 
�������� ������ ����������� ������. ��� ������� �������� ���������� 
����������� ��������� ����� � ������� �����. ��� ����, ����� ���� 
������� �������� ������� �� ����� ������ ���� �� ���� 500 ������.
������ ������  ����� ��������� ���� ����. ��� ������� �������� ��� 
���������� ����� ����������� ����� 5% � ����������� �����, � ����� 
������ � ������� 3% ��� �������� ������ ������ �������. ������������ 
����������� ������ �� ������� �������� � �� ������� �����. ������������ 
����������� ���������� ������� ������� ��������� ������. ��� �������� 
������ ������� � ������ ��������� ����� �������� ���������� � ��������� 
������ � ������� ��������������� ���������� ����� �� �����.  ���� ��� 
���������� �������� ������� ������� ������ ������� ��� � ������ ������� 
�� ������� ������� �� ����� ��� ��������� ������, ���������� ������ 
��������� �� ������.

�������� ����
1.	������ � ���������
2.	������ � ��������

������ � ��������� �������� � ����
"	����� ���� ��������
"	���������� �������
"	�������� ������� (�� ������ �������)
"	���������� �� �����������  ������ ������� � ��������� ������ �����, ��������� ��� �������
"	���������� ����� ������� (�� ������ �������)
"	�������� ������ ������� (�� ������ �������)
"	��������� ������� ������� (�� ������ � �������� �� ������� � � ������� �� ��������)
"	���������� �������� (�� ��� � ��������� �����)

������ � �������� �������� � ����
"	����� ������ �����
"	���������� ������
"	�������� ������ (�� ������ ������)
"	�������������� ������ (�� ������ ������)
*/

using namespace std;

//---------------	����� �����			---------------//
class Service {
private:
	string name;
	double cost;
	bool vip;
public:
	Service (string name, double cost, int vip) {
		this->name = name;
		this->cost = cost;
		if(vip == 1) {
			this->vip = true;	
		}
		this->vip = false;
	}
	
	Service (string name, double cost) {
		this->name = name;
		this->cost = cost;
		this->vip = false;
	}
	
	Service() {
		this->name = "";
		this->cost = 0;
		this->vip = false;
	}
	
	void setName(string name) {
		this->name = name;
	}
	
	string getName() {
		return this->name;
	}
	
	void setCost(double cost) {
		this->cost = cost;
	}
	
	double getCost() {
		return this->cost;
	}
	
	void setVip(int check) {
		if(check == 1) {
			this->vip = true;
		} else {
			this->vip = false;
		}
	}
	
	bool getVip() {
		return this->vip;
	}
	
	virtual void info() = 0;
	
	virtual ~Service() {		
	}
};

//---------------	����������� ������	---------------//
class StandartService :public Service {
public:
	StandartService(string name, double cost) :Service(name, cost) {
	}
	
	StandartService() :Service() {
	}
	
	void info() {
		cout << "������: " << this->getName() << ", ���������: " << this->getCost() << endl;
	}
	
	virtual ~StandartService() {
	}
};

//---------------	������� ������		---------------//
class VipService :public Service {
public:
	VipService(string name, double cost) :Service(name, cost) {
		this->setVip(1);
	}
	
	VipService() :Service() {
		this->setVip(1);
	}
	
	void info() {
		cout << "������ (VIP): " << this->getName() << ", ���������: " << this->getCost() << endl;
	}
	
	virtual ~VipService() {
	}
};

//---------------	����� ��������		---------------//
class Client {
private:
	static double vipLimit;
	static double bonus;
	static double cashback;
	string name;
	string serviceList;
	double balance;
	bool vip;
public:
	Client (string name, string serviceList, double balance, bool vip) {
		this->name = name;
		this->serviceList = serviceList;
		this->balance = balance;
		this->vip = vip;
	}
	
	Client (string name, string serviceList, double balance) {
		this->name = name;
		this->serviceList = serviceList;
		this->balance = balance;
		this->vip = false;
		if (balance >= 500) {
			this->vip = true;
		}
	}
	
	Client (string name, double balance) {
		this->name = name;
		this->serviceList = "---";
		this->balance = balance;
		this->vip = false;
		if (balance >= 500) {
			this->vip = true;
		}
	}
	
	Client() {
		this->name = "";
		this->serviceList = "";
		this->balance = 0;
		this->vip = false;
	}
	
	static void setVipLimit(double vipLimit) {
		Client::vipLimit = vipLimit;
	}
	
	static double getVipLimit() {
		return Client::vipLimit;
	}
	
	static void setBonus(double bonus) {
		Client::bonus = bonus;
	}
	
	static double getBonus() {
		return Client::bonus;
	}
	
	static void setCashback(double cashback) {
		Client::cashback = cashback;
	}
	
	static double getCashback() {
		return Client::cashback;
	}
	
	void setName(string name) {
		this->name = name;
	}
	
	string getName() {
		return this->name;
	}
	
	void setSeriviceList(string service) {
		this->serviceList = service;
	}
	
	string getServiceList() {
		return this->serviceList;
	}
	
	void setBalance(double balance) {
		if(this->vip) {
			this->balance += balance * (Client::bonus / 100 + 1);
		} else {
			this->balance += balance;
		}
	}
	
	double getBalance() {
		return this->balance;
	}
	
	void useService(Service* service) {
		if(service->getVip()) {
			if(this->vip == service->getVip()) {
				if(this->balance >= service->getCost()) {
					this->balance -= service->getCost();
					this->balance += service->getCost() * Client::cashback / 100;
					
					if(this->serviceList != "---") {
						this->serviceList.append(", ");
						this->serviceList.append(service->getName());
					} else {
						this->serviceList.assign("");
						this->serviceList.append(service->getName());
					}
				} else {
					cout << "������������ �������!" << endl;
				}
			} else {
				cout << "��� ������� ������� ������ ����������" << endl;
			}
		} else {
			if(this->balance >= service->getCost()) {
				this->balance -= service->getCost();
				
				if(this->serviceList != "---") {
						this->serviceList.append(", ");
						this->serviceList.append(service->getName());
					} else {
						this->serviceList.assign("");
						this->serviceList.append(service->getName());
					}
			} else {
				cout << "������������ �������!" << endl;
			}
		}
	}
	
	/*
	bool checkVipStatus(Service* service) {
		if (this->vip || this->vip == service->getVip()) {
			return true;
		} else {
			cout << "��� ������� ������� ������ ����������" << endl;
			return false;
		}
	}
	
	bool checkBalance(Service* service) {
		if(this->balance >= service->getCost()) {
			return true;
		} else {
			cout << "������������ �������!" << endl;
			return false;
		}
	}
	*/
	
	void setVip(int check) {
		if(check == 1) {
			this->vip = true;
		} else {
			this->vip = false;
		}
	}
	
	string getVip() {
		if(this->vip) {
			return "VIP";
		} else {
			return "�������";
		}
	}
	
	void changeStatus() {
		if(this->vip) {
			this->vip = false;
		} else {
			this->vip = true;
		}
	}
	
	void info() {
		cout << "���: " << this->getName() << ", ������ �������: " << this->getVip() << ", ������: " << this->getBalance() << ", ������ ��������� �����: " << this->getServiceList() << endl;
	}
	
	friend bool compareClientName(Client*, Client*);
	friend bool compareClientBalance(Client*, Client*);
	
	~Client() {
	}
};

//---------------	Static filed		---------------//
double Client::vipLimit = 500;
double Client::bonus = 5;
double Client::cashback = 3;

//---------------	������ ��� ����������/���������	---//
bool compareClientName(Client* client1, Client* client2) {
	if (client1->getName() > client2->getName()) {
		return false;
	}
	else {
		return true;
	}
}
	
bool compareClientBalance(Client* client1, Client* client2) {
	if (client1->getBalance() > client2->getBalance()) {
		return false;
	}
	else {
		return true;
	}
}

//---------------	���������� ������ ��� ������	---//
int main() {
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);

	int fExit = 1, num, v, sw1, sw2, sw3;
	string name;
	double money;
	vector <Client*> client;
	vector <Service*> service;
	
	cout << "�������� ��� ������� �������� � ������? (1 - ��, 0 - ���)" << endl;
	cin >> num;
	if (num == 1) {
		client.push_back(new Client("������", 800));
		client.push_back(new Client("���", 200));
		client.push_back(new Client("��������", 10));
		
		service.push_back(new StandartService("�������", 100));
		service.push_back(new StandartService("������", 300));
		service.push_back(new VipService("���������", 600));
	}

	do {
		cout << "�������� ����� ����:" << endl;
		cout << "1. ������ � ���������" << endl;
		cout << "2. ������ � ��������" << endl;
		cout << "3. �����" << endl;
		cin >> sw1;
		
		switch (sw1) {
			case 1: {
				cout << "������ � ���������:" << endl;
				cout << "1. ����� ���� ��������" << endl;
				cout << "2. ���������� �������" << endl;
				cout << "3. �������� ������� (�� ������ �������)" << endl;
				cout << "4. ���������� �� �����������  ������ ������� � ��������� ������ �����, ��������� ��� �������" << endl;
				cout << "5. ���������� ����� ������� (�� ������ �������)" << endl;
				cout << "6. �������� ������ ������� (�� ������ �������)" << endl;
				cout << "7. ��������� ������� ������� (�� ������ � �������� �� ������� � � ������� �� ��������)" << endl;
				cout << "8. ���������� �������� (�� ��� � ��������� �����)" << endl;
				cin >> sw2;
				
				switch (sw2) {
					case 1: {
							for (int i = 0; i<client.size(); i++) {
								cout << i+1 << ") ";
								client[i]->info();
							}
					} break;
					
					case 2: {
						cout << "������� ��� �������:" << endl;
						//cin.ignore(INT_MAX, '\n');
						while (cin.get() != '\n');
						getline(cin, name);
						cout << "������� ���������� ������� �� �����:" << endl;
						cin >> money;
						client.push_back(new Client(name, money));
					} break;
					
					case 3: {
						cout << "������� ����� ������� ��� ��������:" << endl;
						cin >> num;
						while(num<1 || num>client.size()) {
							cout << "������������ ����" << endl;
							cin >> num;
						}
						delete client[num - 1];
						client.erase(client.begin() + num -1);
						//client.shrink_to_fit();
					} break;
					
					case 4: {
						cout << "������� ����� �������, �� �������� ������� ����������:" << endl;
						cin >> num;
						while(num<1 || num>client.size()) {
							cout << "������������ ����" << endl;
							cin >> num;
						}
						client[num - 1]->info();
					} break;
					
					case 5: {
						cout << "������� ����� ������� ��� ���������� �����:" << endl;
						cin >> num;
						while(num<1 || num>client.size()) {
							cout << "������������ ����" << endl;
							cin >> num;
						}
						cout << "������� ����� ���������� (������������� �������� ������ ����� � �������):" << endl;
						cin >> money;
						client[num - 1]->setBalance(money);
					} break;
					
					case 6: {
						cout << "������� ����� ������� ��� �������� ������:" << endl;
						cin >> num;
						while(num<1 || num>client.size()) {
							cout << "������������ ����" << endl;
							cin >> num;
						}
						client[num-1]->info();
						
						cout << "�������� ������:" << endl;
						for (int i = 0; i<service.size(); i++) {
							if(client[num-1]->getVip() == "�������" && service[i]->getVip()) {
								cout << i+1 << ") ��� ������� ���� ������� VIP ������ �������������" << endl;;
							} else {
								cout << i+1 << ") " << endl;;
								service[i]->info();
							}
						}
						cin >> v;
						while(v<1 || num>client.size()) {
							cout << "������������ ����" << endl;
							cin >> v;
						}
						
						client[num - 1]->useService(service[v - 1]);
						if(client[num - 1]->getVip() == "VIP" && client[num - 1]->getBalance() < client[num - 1]->getVipLimit()) {
							cout << "������ ������� ���� ���� " << client[num - 1]->getVipLimit() << ", �������� ������ ������ ������� �� �������? (1 - ��, 0 - ���)" << endl;
							cin >> v;
							if(v == 1) {
								client[num - 1]->changeStatus();
							}
						}
					} break;
					
					case 7: {
						cout << "������� ����� ������� ��� ��������� �������:" << endl;
						cin >> num;
						while(num<1 || num>client.size()) {
							cout << "������������ ����" << endl;
							cin >> num;
						}
						client[num - 1]->changeStatus();
					} break;
					
					case 8: {
						cout << "������ � ���������:" << endl;
						cout << "1. ���������� �� ��������" << endl;
						cout << "2. ���������� �� ���������� �������" << endl;
						cin >> sw3;
						
						switch (sw3) {
							case 1: {
								sort(client.begin(), client.end(), compareClientName);
								cout << "���������� �� �������� ���������!" << endl;
							} break;
							
							case 2: {
								sort(client.begin(), client.end(), compareClientBalance);
								cout << "���������� �� ��������� ������� ���������!" << endl;
							} break;
							
							default: {
								cout << "������������ ����� ����!" << endl;
							}
						}
					} break;
					
					default: {
						cout << "������������ ����� ����!" << endl;
					}
				}
			} break;
			
			case 2: {
				cout << "������ � ��������:" << endl;
				cout << "1. ����� ������ �����" << endl;
				cout << "2. ���������� ������" << endl;
				cout << "3. �������� ������ (�� ������ ������)" << endl;
				cout << "4. �������������� ������ (�� ������ ������)" << endl;
				cin >> sw2;
				
				switch (sw2) {
					case 1: {
						for (int i = 0; i<service.size(); i++) {
							cout << i+1 << ") ";
							service[i]->info();
						}
					} break;
					
					case 2: {
						cout << "������� �������� ������:" << endl;
						//cin.ignore(INT_MAX, '\n');
						while (cin.get() != '\n');
						getline(cin, name);
						cout << "������� ���������:" << endl;
						cin >> money;
						cout << "VIP ������? (1 - ��, 0 - ���):" << endl;
						cin >> v;
						if(v == 1 ) {
							service.push_back(new VipService(name, money));
						} else {
							service.push_back(new StandartService(name, money));
						}
					} break;
					
					case 3: {
						cout << "������� ����� ������ ��� ��������:" << endl;
						cin >> num;
						while(num<1 || num>service.size()) {
							cout << "������������ ����" << endl;
							cin >> num;
						}
						delete service[num - 1];
						service.erase(service.begin() + num -1);
						//service.shrink_to_fit();
					} break;
					
					case 4: {
						cout << "������� ����� ������ ��� ��������������:" << endl;
						cin >> num;
						while(num<1 || num>service.size()) {
							cout << "������������ ����" << endl;
							cin >> num;
						}
						
						cout << "������� �������� ������:" << endl;
						//cin.ignore(INT_MAX, '\n');
						while (cin.get() != '\n');
						getline(cin, name);
						cout << "������� ���������:" << endl;
						cin >> money;
						
						cout << "VIP ������? (1 - ��, 0 - ���):" << endl;
						cin >> v;
						if(v == 1 || service[num - 1]->getVip() != v) {
							service[num - 1]->setName(name);
							service[num - 1]->setCost(money);
							//service[num - 1] == typeid(VipService);
						} else {
							service[num - 1]->setName(name);
							service[num - 1]->setCost(money);
							//typeid(*service[num - 1]) == typeid(StandartService);
						}
						
					} break;
					
					default: {
						cout << "������������ ����� ����!" << endl;
					}
				}
			} break;
			
			case 3: {
				fExit = 0;
				for (int i = 0; i < service.size(); i++) {
					delete service[i];
				}
				service.clear();
				//service.shrink_to_fit();
				
				for (int i = 0; i < client.size(); i++) {
					delete client[i];
				}
				client.clear();
				//client.shrink_to_fit();
			} break;
			
			default: {
				cout << "������������ ����� ����!" << endl;
			}
		}
		
		system("pause");
		system("cls");
		
	} while (fExit == 1);
	
	system("pause");
	return 0;
}
