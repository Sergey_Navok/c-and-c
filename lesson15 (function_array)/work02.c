#include<stdio.h>
#include<conio.h>
#include<locale.h>
#include<math.h>
#include<stdlib.h>
#include<time.h>

void inputIntKeyboardArray(int*, int); //��������� ����� ������� � ���-�� ��-���
void outputIntArray(int*, int);
void inputIntRandomArray(int*, int, int, int);

int main()
{
	setlocale(LC_ALL, "rus");
	srand(time(0));
	const int MAX_SIZE=50;
	int a[MAX_SIZE];
	int size;
	
	printf("������� ������ �������:\n");
	scanf("%d", &size);
	while(size<1 || size>MAX_SIZE)
	{
		printf("����������� ����\n");
		scanf("%d", &size);
	}
	
	inputIntKeyboardArray(a, size);
	printf("�������� ������\n");
	outputIntArray(a, size);
	
	inputIntRandomArray(a, size, -50, 50);
	printf("���������� ������\n");
	outputIntArray(a, size);
		
	
	getch();
	return 0;
}

// a - ����� ������ �������
// a+i - ����� �������� ��������
// *(a+i) - �������� ��������

void inputIntKeyboardArray(int * a, int size)
{
	int i;
	
	for(i=0; i<size; i++)
	{
		printf("������� %d ������� �������:\n", i+1);
		scanf("%d", a+i);
	}
}



void outputIntArray(int * a, int size)
{
	int i;
	
	for(i=0; i<size; i++)
	{
		printf("%d ", *(a+i));
	}
	printf("\n");
}


void inputIntRandomArray(int * a, int size, int d1, int d2)
{
	int i;
	
	for(i=0; i<size; i++)
	{
		*(a+i)=rand()%(d2-d1+1)+d1;
	}
}
