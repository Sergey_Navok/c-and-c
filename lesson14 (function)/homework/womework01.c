#include<stdio.h>
#include<conio.h>
#include<locale.h>
#include<math.h>
#include<stdlib.h>
#include<time.h>

/*
1. ������� ������� PowerA3(A, B), ����������� ������ ������� ����� A � ������������ �� � ���������B 
(A - �������, B - ���������; ��� ��������� �������� �������������). 
� ������� ���� ������� ����� ������ ������� ���� ������ �����. 
*/

void powerAB(double, double, double*);

main()
{
	setlocale(LC_ALL, "rus");
	double a, b, tmp, result;
	
	do
	{
		printf("��� ����� ���� ����� ������, ��������� �������� ������!\n");
		printf("������� �����, ������� ����� ���������� � �������:\n");
		tmp=scanf("%lf", &a);
		
		//�������� ����� �� �����
		while(tmp!=1)
		{
			printf("������� �����!\n");
			fflush(stdin);
			tmp=scanf("%lf", &a);
		}
		
		printf("������� �������, � ������� ���� �������� �����:\n");
		tmp=scanf("%lf", &b);
		
		//�������� ����� �� �����
		while(tmp!=1)
		{
			printf("������� �����!\n");
			fflush(stdin);
			tmp=scanf("%lf", &b);
		}
		
		//����� �������
		powerAB(a, b, &result);
		
		//����� �����������
		printf("����� %.2lf � ������� %.2lf = %.2lf\n\n", a, b, result);
	}while(a!=0 || b!=0);
	
	//����� �������
	getch();
	return 0;
}

//�������� (����������) �������
void powerAB(double A, double B, double * result)
{
	*result=pow(A, B);
}

