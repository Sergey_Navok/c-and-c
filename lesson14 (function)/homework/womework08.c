#include<stdio.h>
#include<conio.h>
#include<locale.h>
#include<math.h>
#include<stdlib.h>
#include<time.h>

/*
8. ������� �������Minmax(X, Y), ������������ � ���������� X ����������� �� 
�������� X � Y, � � ���������� Y - ������������ �� ���� �������� (X � Y - ���������). 
��������� ������ ������ ���� �������, ����� ����������� � ������������ �� ������ ����� A, B, C, D.  
*/

void minMax(int*, int*);

main()
{
	setlocale(LC_ALL, "rus");
	int a, b, c, d, tmp;
	
	printf("������� ������ ����� �����:\n");
	tmp=scanf("%d%d%d%d", &a, &b, &c, &d);

	//�������� ����� �� �����
	while(tmp!=4)
	{
		printf("������� ������!\n");
		fflush(stdin);
		tmp=scanf("%d%d%d%d", &a, &b, &c, &d);
	}
	
	//����� �������
	minMax(&a, &b);	
	minMax(&c, &d);
	minMax(&a, &c);
	minMax(&d, &b);
	
	
	printf("������������ �������� = %d\n", a);
	printf("����������� �������� = %d\n", b);
	
	//����� �������
	getch();
	return 0;
}

//�������� (����������) �������
void minMax(int * X, int * Y)
{
	int tmp;
	
	if(*X<*Y)
	{
		tmp=*X;
		*X=*Y;
		*Y=tmp;
	}
}

