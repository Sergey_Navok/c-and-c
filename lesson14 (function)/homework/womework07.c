#include<stdio.h>
#include<conio.h>
#include<locale.h>
#include<math.h>
#include<stdlib.h>
#include<time.h>

/*
7. ������� �������Swap(X, Y), �������� ���������� ���������� X � Y (X � Y - ���������). 
� �� ������� ��� ������ ���������� A, B, C, D ��������������� �������� ���������� 
��������� ���: A � B, C � D, B � C � ������� ����� �������� A, B, C, D. 
*/

void swap(int*, int*);

main()
{
	setlocale(LC_ALL, "rus");
	int a, b, c, d, tmp;
	
	printf("������� ������ ����� �����:\n");
	tmp=scanf("%d%d%d%d", &a, &b, &c, &d);

	//�������� ����� �� �����
	while(tmp!=4)
	{
		printf("������� ������!\n");
		fflush(stdin);
		tmp=scanf("%d%d%d%d", &a, &b, &c, &d);
	}
	
	printf("�������� �������� �����: %d|| %d || %d || %d\n", a, b, c, d);
	
	//����� �������
	swap(&a, &b);
	swap(&c, &d);
	swap(&b, &c);
	
	printf("����� �������� �����:    %d|| %d || %d || %d\n", a, b, c, d);
	
	//����� �������
	getch();
	return 0;
}

//�������� (����������) �������
void swap(int * X, int * Y)
{
	int tmp;
	tmp=*X;
	*X=*Y;
	*Y=tmp;
}

