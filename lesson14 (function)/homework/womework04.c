#include<stdio.h>
#include<conio.h>
#include<locale.h>
#include<math.h>
#include<stdlib.h>
#include<time.h>

/*
4. ������� ������� TrianglePS(a, P, S), ����������� �� ������� a ��������������� 
������������ ��� �������� � ������� (a - �������, P � S - ���������; ��� ��������� 
�������� �������������). � ������� ���� ������� ����� ��������� � ������� ���� 
�������������� ������������� � ������� ���������. 
*/

void trianglePS(double, double*, double*);

main()
{
	setlocale(LC_ALL, "rus");
	double a, p, s, tmp;
	
	do
	{
		printf("��� ����� ����, ��������� �������� ������!\n");
		printf("������� ������� ��������������� ������������:\n");
		tmp=scanf("%lf", &a);
		
		//�������� ����� �� �����
		while(tmp!=1)
		{
			printf("������� �����!\n");
			fflush(stdin);
			tmp=scanf("%lf", &a);
		}
		
		if(a==0 || a<0)
		{
			if(a<0)
			{
				printf("������� ������������ �� ����� ���� �������������!\n");
			}
			else
			{
				printf("���������� ������. ������� ����� �������!\n");
			}
		}
		else
		{
			//����� �������
			trianglePS(a, &p, &s);
			
			//����� �����������
			printf("�������� ������������ = %.2lf\n", p);
			printf("������� ������������ = %.2lf\n", s);
		}
		
		printf("\n--------------------------------------------------\n");
	}while(a!=0);
	
	//����� �������
	getch();
	return 0;
}

//�������� (����������) �������
void trianglePS(double A, double * P, double * S)
{
	*P=(pow(A, 2)*sqrt(3)/4);
	*S=(sqrt(3)/4)*pow(A, 2);
}

