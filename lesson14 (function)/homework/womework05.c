#include<stdio.h>
#include<conio.h>
#include<locale.h>
#include<math.h>
#include<stdlib.h>
#include<time.h>

/*
5. ������� ������� DigitCountSum(K, C, S), ��������� ���������� C ���� 
������ �������������� ����� K, � ����� �� ����� S (K - �������, C � S - 
��������� ������ ����). � ������� ���� ������� ����� ���������� � ����� 
���� ��� ������� �� ���� ������ ����� �����.
*/

void digitCountSum(int, int*, int*);

main()
{
	setlocale(LC_ALL, "rus");
	int k, c, s, tmp;
	
	do
	{
		printf("��� ����� ����, ��������� �������� ������!\n");
		printf("������� �����:\n");
		tmp=scanf("%d", &k);
		
		//�������� ����� �� �����
		while(tmp!=1)
		{
			printf("������� �����!\n");
			fflush(stdin);
			tmp=scanf("%d", &k);
		}
		
		if(k==0 || k<0)
		{
			if(k<0)
			{
				printf("������� ������������� �����!\n");
			}
			else
			{
				printf("���������� ������. ������� ����� �������!\n");
			}
		}
		else
		{
			//����� �������
			digitCountSum(k, &c, &s);
			
			//����� �����������
			printf("���������� ���� � ����� = %d\n", c);
			printf("����� ���� ���� ����� = %d\n", s);
		}
		
		printf("\n--------------------------------------------------\n");
	}while(k!=0);
	
	//����� �������
	getch();
	return 0;
}

//�������� (����������) �������
void digitCountSum(int K, int * C, int * S)
{
	int i, kol=0, sum=0;
	
	for(i=K; i>0; i=i/10)
	{
		sum=sum+i%10;
		kol++;
	}
	
	*C=kol;
	*S=sum;
}

