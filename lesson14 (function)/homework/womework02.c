#include<stdio.h>
#include<conio.h>
#include<locale.h>
#include<math.h>
#include<stdlib.h>
#include<time.h>

/*
2. ������� ������� PowerA234(A, B, C, D), ����������� ������, ������ � ��������� 
������� ����� A � ������������ ��� ������� �������������� � ����������B, C � D 
(A - �������, B, C, D - ���������; ��� ��������� �������� �������������). 
� ������� ���� ������� ����� ������, ������ � ��������� ������� ���� ������ �����. 
*/

void powerA234(double, double*, double*, double*);

main()
{
	setlocale(LC_ALL, "rus");
	double a, b, c, d, tmp;
	
	do
	{
		printf("��� ����� ���� ����� ������, ��������� �������� ������!\n");
		printf("������� �����, ������� ����� ���������� � �������:\n");
		tmp=scanf("%lf", &a);
		
		//�������� ����� �� �����
		while(tmp!=1)
		{
			printf("������� �����!\n");
			fflush(stdin);
			tmp=scanf("%lf", &a);
		}
		
		//����� �������
		powerA234(a, &b, &c, &d);
		
		//����� �����������
		printf("����� %.2lf �� 2� ������� = %.2lf\n", a, b);
		printf("����� %.2lf �  3� ������� = %.2lf\n", a, c);
		printf("����� %.2lf �  4� ������� = %.2lf\n", a, d);
	}while(a!=0);
	
	//����� �������
	getch();
	return 0;
}

//�������� (����������) �������
void powerA234(double A, double * B, double * C, double * D)
{
	*B=pow(A, 2);
	*C=pow(A, 3);
	*D=pow(A, 4);
}

