#include<stdio.h>
#include<conio.h>
#include<locale.h>
#include<math.h>
#include<stdlib.h>
#include<time.h>

/*
3. ������� ������� Mean(X, Y, AMean, GMean), ����������� ������� �������������� 
AMean= (X + Y)/2 � ������� �������������� GMean ���� ������������� ����� X � Y 
(X � Y - �������, AMean� GMean- ���������). � ������� ���� ������� ����� ������� 
�������������� � ������� �������������� ��� ��� (A, B), (A, C), (A, D), ���� ���� A, B, C, D.
*/

void mean(double, double, double*, double*);

main()
{
	setlocale(LC_ALL, "rus");
	double a, b, c, d, aMean, gMean, tmp;
	
	printf("������� ������ ����� (����� 0):\n");
	tmp=scanf("%lf%lf%lf%lf", &a, &b, &c, &d);

	//�������� ����� �� �����
	while(tmp!=4 || (a==0 || b==0 || c==0 || d==0))
	{
		printf("������������ ����!\n");
		fflush(stdin);
		tmp=scanf("%lf%lf%lf%lf", &a, &b, &c, &d);
	}
	
	//����� �������
	mean(a, b, &aMean, &gMean);
	printf("��� ����� %.2lf � %.2lf; ��.���� = %.2lf; ��.���� = %.2lf\n", a, b, aMean, gMean);
	
	mean(a, c, &aMean, &gMean);
	printf("��� ����� %.2lf � %.2lf; ��.���� = %.2lf; ��.���� = %.2lf\n", a, c, aMean, gMean);
	
	mean(a, d, &aMean, &gMean);
	printf("��� ����� %.2lf � %.2lf; ��.���� = %.2lf; ��.���� = %.2lf\n", a, d, aMean, gMean);	
	
	//����� �������
	getch();
	return 0;
}

//�������� (����������) �������
void mean(double X, double Y, double * A, double * G)
{
	*A=(X+Y)/2;
	*G=sqrt(X*Y);
}

