#include<stdio.h>
#include<conio.h>
#include<locale.h>
#include<math.h>
#include<stdlib.h>
#include<time.h>

/*
6. ������� �������InvertDigits(K), �������� ������� ���������� ���� ������ 
�������������� ����� K �� �������� (K - ���������). � ������� ���� ������� 
�������� ������� ���������� ���� �� �������� ��� ������� �� ���� ������ ����� �����. 
*/

void invertDigits(int*);

main()
{
	setlocale(LC_ALL, "rus");
	int k, tmp;
	
	do
	{
		printf("��� ����� ����, ��������� �������� ������!\n");
		printf("������� �����:\n");
		tmp=scanf("%d", &k);
		
		//�������� ����� �� �����
		while(tmp!=1)
		{
			printf("������� �����!\n");
			fflush(stdin);
			tmp=scanf("%d", &k);
		}
		
		if(k==0 || k<0)
		{
			if(k<0)
			{
				printf("������� ������������� �����!\n");
			}
			else
			{
				printf("���������� ������. ������� ����� �������!\n");
			}
		}
		else
		{
			//����� �������
			invertDigits(&k);
			
			//����� �����������
			printf("���������� ����� = %d\n", k);
		}
		
		printf("\n--------------------------------------------------\n");
	}while(k!=0);
	
	//����� �������
	getch();
	return 0;
}

//�������� (����������) �������
void invertDigits(int * K)
{
	int a[10];
	int i, k, j=1, res=0, kol=0;
	k=*K;
	
	for(i=k; i>0; i=i/10)
	{
		a[kol]=i%10;
		kol++;
	}
	
	for(i=kol-1; i>-1; i--)
	{
		res=res+(a[i]*j);
		j=j*10;
	}
	
	*K=res;
}

